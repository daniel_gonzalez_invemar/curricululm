<%-- 
    Document   : list
    Created on : 8/07/2015, 09:26:01 AM
    Author     : usrsig15
--%>
<%

    HttpSession s = request.getSession(false);
    String cedula = (String) s.getAttribute("cedula");
    String nombres = (String) s.getAttribute("nombres");
    String cargo = (String) s.getAttribute("cargo");
    String revisor_curriculum = (String) s.getAttribute("revisor_curriculum");

    if (cedula == null) {
        response.sendRedirect("index.html");
    }


%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Listado de personal</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/signin.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Arapey:400italic,400' rel='stylesheet' type='text/css'>

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery-1.11.3.min.js"></script>     
        <script language="Javascript">

            $(document).ready(function () {
                listEmployee();
                $("#updateList").click(function () {
                    listEmployee();
                });

                $("#showModal").click(function () {
                    $("#message").css("display", "none");
                    $("#RegisterAndActive").show();
                    $("#cedula").prop('disabled', false);
                    $("#AptdatePreselected").hide();
                    cleanFormRegistered();
                });

                $("button[data-dismiss='modal']").click(function () {
                    listEmployee();
                });

                $("#viewall").click(function () {
                    listEmployee();
                });


                function listEmployee() {
                    $.post('ListEmployeesFinishProcess', {
                        action: 'find',
                    }, function (data) {
                        fillTable(data);
                    });
                }


                function cleanFormRegistered() {
                    $("#cedula").val('');
                    $("#nombre").val('');
                    $("#apellido").val('');
                    $("#cargo").val('');
                    $("#clave").val('');
                }

                function registerPreseleccionated() {
                    $.post('RegisteredPreseleccionated', {
                        identification: $("#cedula").val(),
                        name: $("#nombre").val(),
                        lastName: $("#apellido").val(),
                        job: $("#cargo").val(),
                        active:$("#state").val(),
                        clave: $("#clave").val()
                    }, function (data) {

                        $("#message").css("display", "inline");

                        if (data.result == 0) {

                            $("#message").removeClass("alert alert-danger");
                            $("#message").addClass("alert alert-success");

                            $("#message").html("<strong>Se ha registrado de forma exitosa el preseleccionado</strong>");

                        } else {

                            $("#message").addClass("alert alert-danger");
                            $("#message").html("<strong>Error:&nbsp;</strong>" + data.message);
                        }
                    });

                }


                function registerApdating() {
                    $.post('UpdatePreselected', {
                        identification: $("#cedula").val(),
                        name: $("#nombre").val(),
                        lastName: $("#apellido").val(),
                        job: $("#cargo").val(),
                        activo:$("#state").val(),
                        clave: $("#clave").val()
                    }, function (data) {

                        $("#message").css("display", "inline");
                        if (data.result == 0) {

                            $("#message").removeClass("alert alert-danger");
                            $("#message").addClass("alert alert-success");

                            $("#message").html("<strong>Se ha actualizado  de forma exitosa el preseleccionado</strong>");

                        } else {

                            $("#message").addClass("alert alert-danger");
                            $("#message").html("<strong>Error:&nbsp;</strong>" + data.message);
                        }
                    });
                }

                function updatePreseleccionated(cedula) {
                    $.get("GetPreselectedData?cedula=" + cedula, function (data) {        
                        $("#cedula").prop('disabled', true);
                        $("#cedula").val(data.cedula);
                        $("#nombre").val(data.nombre);
                        $("#apellido").val(data.apellido);
                        $("#state").val(data.activo);
                        $("#cargo").val(data.cargo);
                        $("#clave").val(data.clave);

                        $("#RegisterAndActive").hide();
                        $("#AptdatePreselected").show();

                        $("#message").hide();

                        $('#myModal').modal('show');
                    });
                }

                $("#AptdatePreselected").click(function () {
                    registerApdating();
                });


                $("#RegisterAndActive").click(function () {
                    registerPreseleccionated();
                });

                $("#viewPreselected").click(function name() {
                    viewPreselected();

                });

                function viewPreselected() {
                    $.get("ListAllPreselected", function (data) {
                        fillTable(data.result);
                    });
                }


                function addButtonCurriculum(value, row, index) {
                	var action = '<%=revisor_curriculum%>';
                	action = action.toLowerCase();
                    return "<a  id='btnd" + row.cedula + "' href='subir.jsp?cedula=" + row.cedula + "&a=c" +  "&action=" + action + "&t=" + row.tipo + "' target='_blank' class='btn btn-primary'>Ver curriculum</a>"
                }

                window.operateEvents = {
                    'click .updateDataPreseleted': function (e, value, row, index) {
                        updatePreseleccionated(row.cedula);

                    }
                };

                function addButtonOperations(value, row, index) {

                    if (row.tipo != 'Empleados') {
                        return "<span  id='btn_" + row.cedula + "' class='btn btn-primary updateDataPreseleted'>Actualizar</span>";
                    } else {
                        return "-";
                    }

                }

                function fillTable(EmployeeData) {
                    try {
                        $('#employeeList').bootstrapTable('destroy');
                    } catch (Error) {

                    }

                    $('#employeeList').bootstrapTable({
                        columns: [{
                                field: 'estado',
                                title: 'Estado',
                                align: 'center',
                                sortable: true,
                                valign: 'middle'
                            },
                            {
                                field: 'cedula',
                                title: 'cedula',
                                sortable: true
                            }, {
                                field: 'nombre',
                                title: 'Nombre',
                                sortable: true,
                            }, {
                                field: 'apellido',
                                title: 'Apellido',
                                sortable: true,
                            },{
                                field: 'tipo',
                                title: 'Tipo',
                                sortable: true,
                            },
                            {
                                field: 'ver_corriculum',
                                title: 'Ver Curriculum',
                                formatter: addButtonCurriculum
                            },
                            {
                                field: 'Operaciones',
                                title: 'Operaciones',
                                formatter: addButtonOperations,
                                events: operateEvents
                            }
                        ],
                        data: EmployeeData
                    });
                }




            });
        </script>

    </head>

    <body>

        <div class="container">
            <div class="logo"><img src="images/logo-completo.png" style="height:78px" alt="Logo Invemar"></div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li style="margin-left: 5px;margin-right: 5px"><button type="button" class="btn btn-info btn-lg"  id="viewall">Ver Todos</button></li>
                    <li style="margin-left: 5px;margin-right: 5px"><button type="button" class="btn btn-info btn-lg"  id="updateList">Actualizar lista</button></li>
                    <!-- <li style="margin-left: 5px;margin-right: 5px"><button type="button" class="btn btn-info btn-lg"  id="viewPreselected">Ver Preseleccionados</button></li> -->
                    <li style="margin-left: 5px;margin-right: 5px"><button type="button" class="btn btn-info btn-lg"  id="viewPreselected">Ver Candidatos</button></li>
                    <!-- <li><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="showModal">Agregar Preseleccionados</button></li>  -->
                    <li><a href="CerrarSession"><i class="glyphicon glyphicon-log-in"></i> <span> Salir</span></a></li>
                    <!-- <li><a href="#">Ayuda</a></li> -->
                </ul>

            </div>         

            <div class="table-responsive">

                <table id="employeeList"
                       data-search="true"  
                       data-show-toggle="true"
                       data-show-columns="true"                              
                       data-page-size="8"                                                                                 
                       data-minimum-count-columns="2"
                       data-show-pagination-switch="true"                                                                      
                       data-show-footer="true"
                       data-pagination="true"
                       data-toolbar="#toolbar">                                   
                </table>
                <!--<table class="table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-1">
                        <col class="col-xs-1">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Nombre</th>
                            <th>Dependencia</th>
                            <th>Curriculum Vitae.</th>
                        </tr>
                    </thead>
                    <tbody id="list">

                    </tbody>
                </table>-->



            </div>


            <footer>
                <div class="row">
                    <div class="col-md-1 col-sm-3">
                        <img src="images/logo-web.png" style="height:90px" alt="Invemar">
                    </div>
                    <div class="col-md-11 col-sm-8">
                        <address>
                            <strong>Invemar</strong><br>
                            Calle 25 No 2-55 Playa salguero<br>
                            Santa Marta - Colombia<br>
                            <abbr title="Phone">P:</abbr> +57 (5) 432-8600
                        </address>                        
                        <p class="text-right"><small>Powered by: LabSIS and CMC</small></p>
                    </div>
                </div>

            </footer>

        </div> <!-- /container -->


        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Datos del Preseleccionado</h4>
                    </div>
                    <div class="modal-body">
                        <form>

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre">
                            </div>
                            <div class="form-group">
                                <label for="apellido">Apellido</label>
                                <input type="text" class="form-control" id="apellido">
                            </div>
                            <div class="form-group">
                                <label for="cedula">Cedula</label>
                                <input type="text" class="form-control" id="cedula">
                            </div>
                            <div class="form-group">
                                <label for="clave">Clave</label>
                                <input type="password" class="form-control" id="clave" >
                            </div>
                            <div class="form-group">
                                <label for="cargo">Cargo al que aspira</label>
                                <input type="cargo" class="form-control" id="cargo">
                            </div>
                            <div class="form-group">  
                                <label for="state">Estado</label>
                                <select id="state">
                                    <option value="S" selected="">Activado</option>
                                    <option value="N">Desactivado</option>
                                  
                                </select>
                            </div>
                            <button type="button" id="RegisterAndActive" class="btn btn-default"  style="margin-bottom: 30px;font-weight: bold">Registrar y Activar</button>
                            <button type="button" id="AptdatePreselected" class="btn btn-default"  style="margin-bottom: 30px;font-weight: bold">Actualizar</button>
                            <div class="" style="margin-top:20px;display:none" id="message">

                            </div>


                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="font-weight: bold">Cerrar</button>
                    </div>
                </div>

            </div>
        </div>


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">      
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
    </body>
</html>