<%-- 
    Document   : subir
    Created on : 8/07/2015, 09:24:34 AM
    Author     : Daniel Gonzalez 
--%>

<%@page import="co.org.invemar.curriculum.models.Personal"%>
<%@page import="co.org.invemar.curriculum.models.PersonalFactory"%>
<%@page import="co.org.invemar.curriculum.models.PersonalFactoryMethod"%>
<%@page import="org.json.JSONObject"%>
<%@page import="co.org.invemar.curriculum.models.Empleados"%>
<%

    HttpSession s = request.getSession(false);
    String cedula = (String) s.getAttribute("cedula");
    String nombres = (String) s.getAttribute("nombres");
    String cargo = (String) s.getAttribute("cargo");
    String tipo_personal = (String) s.getAttribute("tipo_personal");
    String revisor_curriculum = (String) s.getAttribute("revisor_curriculum");

    if (s == null) {
        response.sendRedirect("/index.html");
    }

    if (cedula == null) {
        response.sendRedirect("index.html");
    }

    //This variable are used by revisor.
    String cedulabyRequest = request.getParameter("cedula");
    String action = request.getParameter("a");
    String tipoPersonal = request.getParameter("t");

    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);   
    
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <META Http-Equiv="Cache-Control" Content="no-cache">
        <META Http-Equiv="Pragma" Content="no-cache">
        <META Http-Equiv="Expires" Content="-1"> 
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="private,no-store,no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Reporte</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Arapey:400italic,400' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script> 
        <script language="Javascript">
        
            $(document).ready(function () {
                var activeGroupName = "";
                
                //var cc_empleado=<%=request.getParameter("cedula")%>

                setLabelPosition();
                controlRol();
                displaySaralyScale();
                resultadoRevisionCriterios();
                estadoInvestigador();
                
                
                function resultadoRevisionCriterios(){
                	var cc_empleado=<%=request.getParameter("cedula")%>
                	$.get("GetRevisionCriterios?cedula=" + cc_empleado, function (data) {
                		//var action='<%=request.getParameter("action")%>'
                        for(j=0;j<data.length; j++){     
                       		if(data[j].ID_CRITERIO==24){
                       			$("#txt_obs_1").val(data[j].OBSERVACIONES);
                       			$("#txt_obs_1").prop('disabled', true);
                       			$("#ck_si_1").prop('disabled', true);
                           		$("#ck_no_1").prop('disabled', true);                       			
                       			if(data[j].CRITERIO_CORRECTO=="Si"){
                              		$("#ck_si_1").prop("checked", true);                              		                              		
                               	 } else{
                               		$("#ck_no_1").prop("checked", true);
                               	 }
                         	} 
							if(data[j].ID_CRITERIO==25){
								$("#txt_obs_2").val(data[j].OBSERVACIONES);
								$("#txt_obs_2").prop('disabled', true);
                       			$("#ck_si_2").prop('disabled', true);
                           		$("#ck_no_2").prop('disabled', true);
                       		 	if(data[j].CRITERIO_CORRECTO=="Si"){
                           			$("#ck_si_2").prop("checked", true);
                            	} else{
                            		$("#ck_no_2").prop("checked", true);
                            	}
                         	} 
							if(data[j].ID_CRITERIO==26){
								$("#txt_obs_3").val(data[j].OBSERVACIONES);
								$("#txt_obs_3").prop('disabled', true);
                       			$("#ck_si_3").prop('disabled', true);
                           		$("#ck_no_3").prop('disabled', true);
                      		 	if(data[j].CRITERIO_CORRECTO=="Si"){
                           			$("#ck_si_3").prop("checked", true);
                            	} else{
                            		$("#ck_no_3").prop("checked", true);
                            	}
                          	} 
							if(data[j].ID_CRITERIO==27){
								$("#txt_obs_4").val(data[j].OBSERVACIONES);
								$("#txt_obs_4").prop('disabled', true);
                       			$("#ck_si_4").prop('disabled', true);
                           		$("#ck_no_4").prop('disabled', true);
                       		 	if(data[j].CRITERIO_CORRECTO=="Si"){
                            		$("#ck_si_4").prop("checked", true);
                             	} else{
                             		$("#ck_no_4").prop("checked", true);
                             	}
                          	}
							if(data[j].ID_CRITERIO==28){
								$("#txt_obs_5").val(data[j].OBSERVACIONES);
								$("#txt_obs_5").prop('disabled', true);
                       			$("#ck_si_5").prop('disabled', true);
                           		$("#ck_no_5").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_5").prop("checked", true);
                              	} else{
                              		$("#ck_no_5").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==29){
								 $("#txt_obs_6").val(data[j].OBSERVACIONES);
								 $("#txt_obs_6").prop('disabled', true);
	                       		 $("#ck_si_6").prop('disabled', true);
	                           	 $("#ck_no_6").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_6").prop("checked", true);
                              	} else{
                              		$("#ck_no_6").prop("checked", true);
                             	}
                          	 } 
							 if(data[j].ID_CRITERIO==30){
								 $("#txt_obs_7").val(data[j].OBSERVACIONES);
								 $("#txt_obs_7").prop('disabled', true);
	                       		 $("#ck_si_7").prop('disabled', true);
	                           	 $("#ck_no_7").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_7").prop("checked", true);
                              	} else{
                              		$("#ck_no_7").prop("checked", true);
                              	}
                          	 }
							 if(data[j].ID_CRITERIO==31){
								 $("#txt_obs_8").val(data[j].OBSERVACIONES);
								 $("#txt_obs_8").prop('disabled', true);
	                       		 $("#ck_si_8").prop('disabled', true);
	                           	 $("#ck_no_8").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_8").prop("checked", true);
                              	} else{
                              		$("#ck_no_8").prop("checked", true);
                              	}
                          	 }
							 if(data[j].ID_CRITERIO==33){
								 $("#txt_obs_9").val(data[j].OBSERVACIONES);
								 $("#txt_obs_9").prop('disabled', true);
	                       		 $("#ck_si_9").prop('disabled', true);
	                           	 $("#ck_no_9").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_9").prop("checked", true);
                              	} else{
                              		$("#ck_no_9").prop("checked", true);
                              	}
                          	 }
							 if(data[j].ID_CRITERIO==32){
								 $("#txt_obs_10").val(data[j].OBSERVACIONES);
								 $("#txt_obs_10").prop('disabled', true);
	                       		 $("#ck_si_10").prop('disabled', true);
	                           	 $("#ck_no_10").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_10").prop("checked", true);
                              	} else{
                              		$("#ck_no_10").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==42){
								 $("#txt_obs_11").val(data[j].OBSERVACIONES);
								 $("#txt_obs_11").prop('disabled', true);
	                       		 $("#ck_si_11").prop('disabled', true);
	                           	 $("#ck_no_11").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_si_11").prop("checked", true);
                              	} else{
                              		$("#ck_no_11").prop("checked", true);
                              	}
                          	 } 
							 
							 //PLANEACION
							 if(data[j].ID_CRITERIO==34){
								 $("#txt_obs_inves_1").val(data[j].OBSERVACIONES);
								 $("#txt_obs_inves_1").prop('disabled', true);
	                       		 $("#ck_inves_si_1").prop('disabled', true);
	                           	 $("#ck_inves_no_1").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_inves_si_1").prop("checked", true);
                              	} else{
                              		$("#ck_inves_no_1").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==35){
								 $("#txt_obs_inves_2").val(data[j].OBSERVACIONES);
								 $("#txt_obs_inves_2").prop('disabled', true);
	                       		 $("#ck_inves_si_2").prop('disabled', true);
	                           	 $("#ck_inves_no_2").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_inves_si_2").prop("checked", true);
                              	} else{
                              		$("#ck_inves_no_2").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==36){
								 $("#txt_obs_inves_3").val(data[j].OBSERVACIONES);
								 $("#txt_obs_inves_3").prop('disabled', true);
	                       		 $("#ck_inves_si_3").prop('disabled', true);
	                           	 $("#ck_inves_no_3").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_inves_si_3").prop("checked", true);
                              	} else{
                              		$("#ck_inves_no_3").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==37){
								 $("#txt_obs_inves_4").val(data[j].OBSERVACIONES);
								 $("#txt_obs_inves_4").prop('disabled', true);
	                       		 $("#ck_inves_si_4").prop('disabled', true);
	                           	 $("#ck_inves_no_4").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_inves_si_4").prop("checked", true);
                              	} else{
                              		$("#ck_inves_no_4").prop("checked", true);
                              	}
                          	 } 
							 if(data[j].ID_CRITERIO==38){
								 $("#txt_obs_inves_5").val(data[j].OBSERVACIONES);
								 $("#txt_obs_inves_5").prop('disabled', true);
	                       		 $("#ck_inves_si_5").prop('disabled', true);
	                           	 $("#ck_inves_no_5").prop('disabled', true);
                        		 if(data[j].CRITERIO_CORRECTO=="Si"){
                             		$("#ck_inves_si_5").prop("checked", true);
                              	} else{
                              		$("#ck_inves_no_5").prop("checked", true);
                              	}
                          	 } 							 
                       	 }
                    });
                }
                
                function controlRol() {
                    var revisor_curriculum = '<%=revisor_curriculum%>';
                    var tipoPersonal = '<%=tipoPersonal%>';
                    var tipo_personal = '<%=tipo_personal%>';
                    
                    if (revisor_curriculum == 'S') {
                        showRevisorItem();
                        //$("#MANEJO_DATOS").hide();
                        if (tipoPersonal == 'Empleados') {
                            showStaffItem('S');
                        } else {
                            hideStaffItem();
                        }
                    } else if (revisor_curriculum == 'C') {//USUARIO DE CDO - SOLO VISUALIZA PRODUCCIÓN CIENTÍFICA
                    	$("#MANEJO_DATOS").hide();
                    	showCDO();
                    } else if (revisor_curriculum == 'P') {//USUARIO DE PLA - SOLO VISUALIZA INVESTIGACIÓN E INNOVACIÓN
                    	$("#MANEJO_DATOS").hide();
                    	showPLA();
                    }else if (revisor_curriculum == 'T') {//USUARIO DE TAL 
                    	//$("#MANEJO_DATOS").hide();
                    	$("#INVESTIGACION_INNOVACION").hide();
                    	$("#CONCEPTOS_TECNICOS").hide();
                    	$("#PRODUCCION_CIENTIFICA").hide();
                    	$("#EXPERIENCIA_LABORAL").hide();
                    	$("#ESTUDIOS").hide();
                    	$("#HABILITAR_INVESTIGADOR").hide();
                    	onlyCDO();
                    	onlyPLA();
                    }
                    else {
                        hideRevisorItem();
                        if (tipo_personal == 'Empleados') {
                            showStaffItem('E');
                        } else {
                            hideStaffItem();
                        }
                    }
                }
                
                
                function displaySaralyScale() {
                    var cedula = <%=cedulabyRequest%>

                    $('#scoreTotal').html("0");
                    $('#scoreTotalAlert2').html("0");
                    $.get("getsalaryscalepersonal?cedula=" + cedula, function (data) {
                        $('#labelSalaryScale').show();
                        $('#scalafonAlert1').html(data.salaryScale.scale);
                        $('#scalafonAlert2').html(data.salaryScale.scale);
                        $('#scoreTotal').html(data.scoreScale.totalscore);
                        $('#scoreTotalAlert2').html(data.scoreScale.totalscore);
                    });
                }

                function setLabelPosition() {
                    var tipoPersonal = '<%=tipoPersonal%>';
                    var tipo_personalLogeado = '<%=tipo_personal%>';
                    var revisor_curriculum = '<%=revisor_curriculum%>';

                    if (tipo_personalLogeado == 'Empleados') {
                        $('#labelPosition').html('Cargo Actual: ');

                    } else if (tipo_personalLogeado == 'Preseleccioanados') {
                        $('#labelPosition').html('Cargo al que aspira: ');
                    }

                    if (revisor_curriculum == 'S') {
                        if (tipoPersonal == 'Empleados') {
                            $('#labelPosition').html('Cargo Actual: ');
                        } else if (tipoPersonal == 'Preseleccioanados') {
                            $('#labelPosition').html('Cargo al que aspira: ');
                        }
                    }
                }
                
                
                function estadoInvestigador() {
                    var cedula = <%=cedulabyRequest%>
                    var estado = "";
                    var autorizacion = "";

                    $.get("GetEstadoInvestigador?cedula=" + cedula, function (data) {
                        estado = data.ESTADO;
                        autorizacion = data.AUTORIZACION;
                                                
                        if(estado == 'I'){
                        	$("#HABILITAR_INVESTIGADOR").hide();
                        	$("#ck_autorizo").prop('disabled', false);
                        	$("#ck_no_autorizo").prop('disabled', false);
                        }else{
                        	$("#ck_autorizo").prop('disabled', true);
                        	$("#ck_no_autorizo").prop('disabled', true);
                        }
                        
                        if(autorizacion == "Si"){
                        	$("#ck_autorizo").prop("checked", true);                         	
                        }else if(autorizacion == "No"){
                        	$("#ck_no_autorizo").prop("checked", true);                        	
                        }
                        
                    });                   
                }

                function showRevisorItem() {
                    $('.score').show();
                    $('.scoreProd').show();
                }
                function hideRevisorItem() {
                    $('.score').hide();
                    $('.scoreProd').hide();
                }


                function showStaffItem(rol) {
                    $("#escalafonactual").show();
                    $("#seccionConceptosTecnicos").show();
                    $("#iecuc").show();
                    $("#iesa").show();  
                    if(rol=='E'){
                    	$("#INVESTIGACION_INNOVACION").hide();
                    	$("#HABILITAR_INVESTIGADOR").hide();
                    }                    
                    onlyCDO();
                    onlyPLA();
                }
                function hideStaffItem() {
                    $("#escalafonactual").hide();
                    $("#iecuc").hide();
                    $("#iesa").hide();                    
                    $("#seccionConceptosTecnicos").hide();                    
					onlyCDO();
					onlyPLA();
                }
                
                function showCDO() {//PRODUCCIÓN CIENTÍFICA
                	$("#seccionStudios").hide();
                	$("#seccionConceptosTecnicos").hide();
                	$("#seccionExperiencia").hide();    
                	$("#seccionInnovacion").hide(); 
                	$("#escalafonactual").hide(); 
                	$("#PRODUCCION_CIENTIFICA").hide();
                	$("#VERIFICACION_INVESTIGACION_INNOVACION").hide();
                	$("#HABILITAR_INVESTIGADOR").hide();
                	$("#iedu").hide();
                    $("#iexp").hide();
                    $("#iproc").hide();
                    $("#iecuc").hide();
                    $("#iiei").hide();
					$("#iesa").hide();
                }
                
                function onlyCDO() {
	                $("#correcto_prod").hide();
	                $("#correcto_arti_1").hide();
	                $("#correcto_arti_2").hide();
	                $("#correcto_arti_3").hide();
	                $("#correcto_arti_4").hide();
	                $("#correcto_arti_5").hide();
	                $("#correcto_arti_6").hide();
	                $("#correcto_arti_7").hide();
	                $("#correcto_arti_8").hide();
	                $("#correcto_arti_9").hide();
	                $("#correcto_arti_10").hide();
	                $("#correcto_arti_11").hide();
	                $("#VERIFICACION_PRODUCCION_CIENTIFICA").hide();
	                $("#observaciones_prod").hide();
	                $("#observacion_1").hide();
	                $("#observacion_2").hide();
	                $("#observacion_3").hide();
	                $("#observacion_4").hide();
	                $("#observacion_5").hide();
	                $("#observacion_6").hide();
	                $("#observacion_7").hide();
	                $("#observacion_8").hide();
	                $("#observacion_9").hide();
	                $("#observacion_10").hide(); 
	                $("#observacion_11").hide();
                }
                
                
                function showPLA() {//INVESTIGACIÓN E INNOVACIÓN
                	$("#seccionStudios").hide();
                	$("#seccionConceptosTecnicos").hide();
                	$("#seccionExperiencia").hide();    
                	$("#seccionProduccion").hide(); 
                	$("#escalafonactual").hide(); 
                	$("#PRODUCCION_CIENTIFICA").hide();
                	$("#VERIFICACION_PRODUCCION_CIENTIFICA").hide();
                	$("#HABILITAR_INVESTIGADOR").hide();
                	$("#finishprocess").hide();
                	$("#INVESTIGACION_INNOVACION").hide();
                	$("#iedu").hide();
                    $("#iexp").hide();
                    $("#iproc").hide();
                    $("#iecuc").hide();
                    $("#iiei").hide();
					$("#iesa").hide();
                }
                
                function onlyPLA() {
                	$("#correcto_inves").hide();
                	$("#correcto_inves_1").hide();
                	$("#correcto_inves_2").hide();
                	$("#correcto_inves_3").hide();
                	$("#correcto_inves_4").hide();
                	$("#correcto_inves_5").hide();
                	$("#correcto_inves_6").hide();
	                $("#VERIFICACION_INVESTIGACION_INNOVACION").hide();  
                	$("#observaciones_inves").hide();
                	$("#observacion_inves_1").hide();
                	$("#observacion_inves_2").hide();
                	$("#observacion_inves_3").hide();
                	$("#observacion_inves_4").hide();
                	$("#observacion_inves_5").hide();
                	$("#observacion_inves_6").hide();
                }


                function setCellStyle(value, row, index, field) {
                    return {
                        css: {"color": "blue", "font-size": "12px", cursor: "pointer"}
                    };
                }


                function sumaryItem(ArrayItem) {
                	console.log("Sumary Item");
                	console.log('*********===================',ArrayItem.length)                	
                    var scoreTotal = 0;

                    $.each(ArrayItem, function (index, value) {
                    	//console.log('>>>>>>>>VALORES>>>>>>>>>>>',value.value)
                    	//console.log('>>>>>>>>SOPORTES>>>>>>>>>>>',value.soportes)
                        scoreTotal = scoreTotal + (value.value*value.soportes);                    	
                    	//console.log('>>>>>>>>scoreTotal>>>>>>>>>>>',scoreTotal);
                    });
                    //console.log('>>>>>>>>scoreTOTAL1>>>>>>>>>>>',scoreTotal);
                    $('#resultTotal').html(scoreTotal);
                    //console.log('>>>>>>>>scoreTOTAL>>>>>>>>>>>',scoreTotal);
                    return scoreTotal;
                }

                $('#critheriumTable').on('page-change.bs.table', function (e, size, number) {                    
                })

                $('#critheriumTable').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
                    var selectedArray = $('#critheriumTable').bootstrapTable('getSelections');
                    var listChitherium_ = "";
                    var cedula_ = '<%=cedulabyRequest%>';
                    var scoreTotal = 0;
                    var cantidad_soportes_ = "";
                    $.each(selectedArray, function (index, value) {
                    	//console.log('++++++++++++++++++++++++++++++++++++++++++++++++', $("input#txt_"+value.id).val())
                        listChitherium_ = listChitherium_ + "," + value.id;
                        cantidad_soportes_ = cantidad_soportes_ + "," + $("input#txt_"+value.id).val();
                    });

                    $.post('registerIdUsuerAndCritherium', {
                        cedula: cedula_,
                        ListCritherium: listChitherium_,
                        groupName: activeGroupName, 
                        cantidad_soportes: cantidad_soportes_
                    }, function (data) {
                    	//console.log('++++++++++++++++++data++++++++++++++++++++++++', data)
                        if (data == 0) {
                        	$.each(selectedArray, function (index, value) {
	                        	selectedArray.find(function (f){
	                        		if (f.id==value.id){
	                        			f.soportes = $("input#txt_"+value.id).val();
	                        		}
	                        	});
                        	});
                        	//console.log('selectedArray>>>>>>>', selectedArray)
                            $('#resultTotal').html(sumaryItem(selectedArray));
                        	//console.log('***************',sumaryItem(selectedArray))
                        } else {
                            $('#resultTotal').html('Error registrando los puntajes');
                        }
                    });
                });


                function checkItem(data) {
                    var tableOptions = $('#critheriumTable').bootstrapTable('getOptions');   
                    var ItemSelected = [];
                    
                    for(x=0;  x<data.length; x++){
                    	 $.each(tableOptions.data, function (index, value) {
                    		 if (value.estado == 'checked') {  
                                 $('#critheriumTable').bootstrapTable('check', index);   
                                 cant = data[x].soportes;
                                 //console.log("Cantidaaaaad de BD...>", cant);
                                 if(cant==0){
                                	 cant=1;
                                 }
                                 $("input#txt_"+data[x].id).val(cant);
                                 //ItemSelected.push({id: value.id, name: value.name, value: value.value, cantidad: $("input#txt_"+value.id).val()});
                                 ItemSelected.push({id: value.id, name: value.name, value: value.value, cantidad: cant});
								}
						 });
					}
                    sumaryItem(ItemSelected);
                }

                $('.score').on('click', function (event) {
                    $('#resultTotal').html('0');
                    //$('#puntajeMaximo').html(''+event.target.dataset.puntmax);
                    var cedula_ = '<%=cedulabyRequest%>';

                    activeGroupName = event.target.id;
                    $.get("ListCritheriumByGroup?cedula=" + cedula_ + "&group=" + event.target.id, function (data) {
                        fillCritheriumTable(data.critherium, false);
                        add_cantidad(data.critherium);
                        checkItem(data.critherium);
                    });

                    $('#modalScoring').modal('show');
                    $("#texto_cantidad").css('display', 'none');

                });
                
                
                $('.scoreProd').on('click', function (event) {
                    $('#resultTotal').html('0');
                    var cedula_ = '<%=cedulabyRequest%>';
					
                    activeGroupName = event.target.id;
                    console.log("activeGroupName...", activeGroupName);
                    $.get("ListCritheriumByGroup?cedula=" + cedula_ + "&group=" + event.target.id, function (data) {
                        fillCritheriumTable(data.critherium, true);
                        add_cantidad(data.critherium);
                        checkItem(data.critherium);
                    });                    
                    
                    $('#modalScoring').modal('show');
                    $("#texto_cantidad").css('display', 'block');

                });
                
                
                $('.scoreInves').on('click', function (event) {
                    $('#resultTotal').html('0');
                    var cedula_ = '<%=cedulabyRequest%>';
					
                    activeGroupName = event.target.id;
                    console.log("activeGroupName...", activeGroupName);
                    $.get("ListCritheriumByGroup?cedula=" + cedula_ + "&group=" + event.target.id, function (data) {
                        fillCritheriumTable(data.critherium, true);
                        add_cantidad(data.critherium);
                        checkItem(data.critherium);
                    });                    
                    
                    $('#modalScoring').modal('show');
                    $("#texto_cantidad").css('display', 'block');

                });

                
                $('.habilitarUsuario').on('click', function (event) {
                	var cedula_ = '<%=cedulabyRequest%>';
                	//console.log(cedula_, "****************************");
                	$.get("ActivarInvestigador?cedula=" + cedula_ + "&estado=I", function (data) {
                		
                    });  
                	alert("El investigador puede cargar soportes nuevamente!");
                });
                
		
                function fillCritheriumTable(critheriumData, view_cant) {
                    try {
                        $('#critheriumTable').bootstrapTable('destroy');
                    } catch (Error) {

                    }

                    $('#critheriumTable').bootstrapTable({
                        columns: [{
                                field: 'state',
                                checkbox: true,
                                align: 'center',
                                valign: 'middle'
                            },
                            {
                                field: 'id',
                                title: 'ID',
                                sortable: true,
                                class: 'id_criterio'                                
                            }, {
                                field: 'name',
                                title: 'Descripcion',
                                sortable: true,
                                cellStyle: setCellStyle
                            }, {
                                field: 'value',
                                title: 'Puntaje',
                                sortable: true
                            }, {
                                field: 'cant',
                                title: 'Cantidad',
                                class: 'input_cantidad ' + ((!view_cant) ? 'hidden' : ''),
                                visible: true
                            }
                            ],
                        data: critheriumData
                    });
                }
                
				function add_cantidad(critherium){
					for(x=0;  x<critherium.length; x++){
						 $("td.id_criterio").each(function(){
							 if($(this).text() == critherium[x].id){
									//console.log('id for = ' +  critherium[x].id)
									//console.log('>>>'+ $(this).text());
									var input_html = "<input type='text' name='cantidad_"+critherium[x].id+"' value='1' id='txt_"+critherium[x].id+"' />";
									$(this).closest("tr").find("td.input_cantidad").append(input_html);
								}
						 });
					}
					
				}
				

                $("#RegisterAndActive").click(function () {
                    registerPreseleccionated();
                });

                $("#showSalaryScale").click(function () {
                    displaySaralyScale();
                });


                $("#ck67").prop('disabled', true);

                $("#escalafon").change(function () {
                    var value = $("#escalafon").val();

                    if (value != "-1") {
                        $("#ck67").show();
                        $("#67").show();
                    } else {
                        $("#ck67").hide();
                        $("#67").hide();
                    }

                });


                $("input[name='tiempoexperiencia']").click(function () {
                    $("#ck67").removeAttr("disabled");

                });

                $('input[type="file"]').prop("disabled", true);


                $('input[type="file"]').ajaxfileupload({
                    'action': 'UploadFile',
                    'onComplete': function (response) {

                        $('#mymodal').modal('hide');

                        if (response.cedula == null) {
                            window.location.replace("index.html");
                        }
                        $("#link" + response.criterio).show();
                        $("#message" + response.criterio).hide();
                        $("#ch" + response.criterio).prop("checked", true);
                        $("#ck" + response.criterio).prop("checked", true);
                        $("#ck" + response.criterio).prop('disabled', true);
                        $("#" + response.criterio).prop('disabled', true);
                        $("#btnd" + response.criterio).show();
                        $("#link" + response.criterio).attr("href", response.file);
                        $("#" + response.criterio).hide();

                        if (response.criterio == "67") {
                            experienciaInvemar = $('input[type="radio"][name="tiempoexperiencia"]:checked').val();
                            _escalafon = $("#escalafon option:selected").val();

                            $.post('UpdataExtraParameter', {
                                criterio: response.criterio,
                                param1: experienciaInvemar,
                                param2: _escalafon
                            }, function (data) {

                            });
                        }
                    },
                    'onStart': function () {
                        $('#mymodal').modal('show');
                    },
                });


                $.post('ListCritheriumFile', {
                    cedula: '<%=cedula%>',
                }, function (data) {					
                    if (data[0] != undefined && data[0].estado == "F") {
                        $("#finishprocess").prop('disabled', true);
                        $("input:checkbox").prop('disabled', true);
                        $("#escalafon").prop('disabled', true);
                        $("input:radio[name='tiempoexperiencia']").prop('disabled', true);
                    }
                    
                    jQuery.each(data, function (index, item) {
                        $("#link" + item.idcriterio).show();
                        $("#btnd" + item.idcriterio).show();
                        $("#" + item.idcriterio).hide();
                        $("#ck" + item.idcriterio).prop("checked", true);
                        $("#ck" + item.idcriterio).prop('disabled', true);
                        //$("#link" + item.idcriterio).attr("href", item.rutasoporte + "?buster=" + Math.random());
                        $("#link" + item.idcriterio).attr("href", item.rutasoporte);
                        if (item.estado == "F") {
                            $("#btnd" + item.idcriterio).hide();
                        }
                        if (item.idcriterio == 67) {
                            $("#escalafon").val(item.param2);
                            $("input:radio[name='tiempoexperiencia'][value=" + item.param1 + "]").attr('checked', true);
                        }

                    });
                });

            <% 
                //System.out.println(action);
                
                if (action != null && action.equalsIgnoreCase("c")) {
                    PersonalFactoryMethod factory = new PersonalFactory();
                    Personal personal = factory.createPersonal(tipoPersonal);
                    JSONObject json = personal.getDatos(cedulabyRequest);
                    cedula = json.getString("cedula");
                    nombres = json.getString("nombre") + " " + json.getString("apellido");
                    cargo = json.getString("cargo");
            %>


                $.post('ListCritheriumFile', {
                    cedula: '<%=cedulabyRequest%>',
                }, function (data) {
                    jQuery.each(data, function (index, item) {
                        $("#link" + item.idcriterio).show();
                        $("#btnd" + item.idcriterio).hide();
                        $(item.idcriterio).removeAttr("disabled");
                        $("#ck" + item.idcriterio).prop("checked", true);
                        $("#ck" + item.idcriterio).prop('disabled', true);
                        //$("#link" + item.idcriterio).attr("href", item.rutasoporte + "?buster=" + Math.random());
                        $("#link" + item.idcriterio).attr("href", item.rutasoporte);


                        if (item.idcriterio == 67) {
                            $("#escalafon").val(item.param2);
                            $("#escalafon").prop('disabled', true);
                            $("input:radio[name='tiempoexperiencia'][value=" + item.param1 + "]").attr('checked', true);
                            $("input:radio[name='tiempoexperiencia']").prop('disabled', true);
                        }
                    });
                });

                $('#finishprocess').hide();
                $('input[type="file"]').hide();
                $('input[type="file"]').hide();

            <%}%>


                $("input[type='checkbox']").click(function (event) {
                    var idelement = event.target.id;
                    var inputId = idelement.replace("ck", "");
                    $("#" + inputId).removeAttr('disabled');
                    state = $("#" + inputId).prop('checked');

                });
                /*$("#l2012").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2012.pdf?key=' + Math.random(), '_blank');
                });

                $("#l2013").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2013.pdf?key=' + Math.random(), '_blank');
                });
                $("#l2014").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2014.pdf?key=' + Math.random(), '_blank');
                });*/
                $("#l2015").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2015.pdf?key=' + Math.random(), '_blank');
                });
                $("#l2016").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2016.pdf?key=' + Math.random(), '_blank');
                });
                $("#l2017").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2017.pdf?key=' + Math.random(), '_blank');
                });
                $("#l2018").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/2018.pdf?key=' + Math.random(), '_blank');
                });
                $("#l2014-2019").click(function (event) {
                    window.open('http://buritaca.invemar.org.co/Curriculum/help/20142019.pdf?key=' + Math.random(), '_blank');
                });


                $("#finishprocess").click(function (event) {
                    var finishProces = true;
                    var con = 0;

                    $('input:checkbox:checked').each(function () {
                    	con +=1;
                    	console.log("Contador....... ",con)
                        var idElement = $(this).attr('id');
                        var id = idElement.replace("ck", "");
                        //console.log("######" , id.indexOf("_si_"))
                        
                        if(id.indexOf("_si_")){                        	
                        }else if(id.indexOf("_no_")){                        	
                        }else{
                             if ($("#link" + id).attr('href').length == 0){
                                $("#message" + id).show();
                                $("#message" + id).html("<strong>Nota </strong> Falta subir el soporte");
                                finishProces = false;
                            }
                        }            
                    });

                    if (finishProces) {
                        var r = confirm("Esta seguro de finalizar y someter su hoja de vida!");
                        var autoriza="";
                        
                        if (r) {
                            $("[data-deleting-text='Deleting...']").prop('disabled', true);
                            $("[data-deleting-text='Deleting...']").hide();
                            $("input:checkbox").prop('disabled', true);

                            if(document.getElementById("ck_autorizo").checked){
                            	autoriza = "Si";
                            }else if(document.getElementById("ck_no_autorizo").checked){
                            	autoriza = "No";
                            }
                            else{
                            	autoriza = "Si";
                            }
                            
                            
                            $.post('FinishProces', {                            	
                                cedula: '<%=cedula%>',
                                autorizacion: autoriza,
                            }, function (data) {

                                if (data.state == "notlogged") {
                                    window.location.replace("index.html");

                                }
                                if (data.result == "1") {
                                    $('#modalmessage').modal('show');
                                    $('.modal-body').html("La hoja de vida ha sido reportada con &eacute;xito.");
                                }
                                

                            });
                        } else {

                        }

                    } else {
                        $('#modalmessage').modal('show');
                        $('.modal-body').html("Algunos criterios no tienen soporte. Por favor, suba el soporte.");
                    }
                });
            });


            function ActiveElement(item) {
                var vector = ["iedu", "iedu", "iproc", "iecuc", "iiei", "iesa", "iexp"];
                for (var i = 0; i <= vector.length; i++) {
                    if (item == vector[i]) {
                        $("#" + vector[i]).addClass("active");
                    } else {
                        $("#" + vector[i]).removeClass("active");
                    }
                }
            }

            function noBack(){
                window.history.forward();
            }

            function deleteCritherium(criterio) {
                $.post('DeleteCritherium', {
                    critherium: criterio,
                }, function (data) {

                    if (data.result == 1) {
                        $("#link" + criterio).hide();
                        $("#btnd" + criterio).hide();
                        $("#ck" + criterio).prop("checked", false);
                        $("#ck" + criterio).removeAttr('disabled');
                        $("#link" + criterio).attr("href", "");
                        $("#" + criterio).show();

                        if (criterio == 67) {
                            var value = $("#escalafon").val();
                            if (value != "-1") {
                                $("#ck67").show();
                                $("#67").show();
                            } else {
                                $("#ck67").hide();
                                $("#67").hide();
                            }
                        }

                    }

                    if (data.state == "notlogged") {
                        window.location.replace("index.html");
                    }
                });
            }
            
            
            var host = "";
        	var to = "richard.restrepo@invemar.org.co";
        	//var to = "";
        	var from = "";
        	var subject = "";
        	var msj = "";
			                        
            function sendEmailJefe(host, from, to, subject, msj){
            	var cc_empleado=<%=request.getParameter("cedula")%>
            	var action='<%=request.getParameter("action")%>'
            	                                      
            	
            	
            	$.get("GetInfoRevisor?action=" + action, function (data) {//Obtener nombre y correo del revisor
            		revisor = data.REVISOR;
            		cc_revisor = data.CC_REVISOR;
            		from = data.EMAIL_REVISOR;
            		
	            	 $.get("GetEmailJefe?cedula=" + cc_empleado, function (data) {//Obtener nombre y correo del jefe y del empleado 
	            		jefe = data.NOMBRE_JEFE 
	            		empleado = data.EMPLEADO
	            		console.log("data: ", data)
	                 	to = data.EMAIL_JEFE;
	            	 	subject = "Revisión curriculum - " + empleado;

                		articulos = "";
                		
	            		if(action=="c"){
	                    	if(document.getElementById("ck_si_1").checked){
	                    		articulos += "- Artículo científico en revista internacional indexada como primer autor - ";
	                    		if(document.getElementById("txt_obs_1").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_1").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 24, 'Si', document.getElementById("txt_obs_1").value);
	                    	}else if(document.getElementById("ck_no_1").checked){
	                    		/*if(document.getElementById("txt_obs_1").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_1").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 24, 'No', document.getElementById("txt_obs_1").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_2").checked){
	                    		articulos += "- Artículo científico en revista internacional indexada como co-autor - ";
	                    		if(document.getElementById("txt_obs_2").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_2").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 25, 'Si', document.getElementById("txt_obs_2").value);
	                    	}else if(document.getElementById("ck_no_2").checked){
	                    		/*if(document.getElementById("txt_obs_2").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_2").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 25, 'No', document.getElementById("txt_obs_2").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_3").checked){
	                    		articulos += "- Artículo científico en revista nacional indexada como primer autor - ";
	                    		if(document.getElementById("txt_obs_3").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_3").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 26, 'Si', document.getElementById("txt_obs_3").value);
	                    	}else if(document.getElementById("ck_no_3").checked){
	                    		/*if(document.getElementById("txt_obs_3").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_3").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 26, 'No', document.getElementById("txt_obs_3").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_4").checked){
	                    		articulos += "- Artículo científico en revista nacional indexada como co-autor - ";
	                    		if(document.getElementById("txt_obs_4").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_4").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 27, 'Si', document.getElementById("txt_obs_4").value);
	                    	}else if(document.getElementById("ck_no_4").checked){
	                    		/*if(document.getElementById("txt_obs_4").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_4").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 27, 'No', document.getElementById("txt_obs_4").value);
	                    	}
	                    
	                    	if(document.getElementById("ck_si_5").checked){
	                    		articulos += "- Informes técnicos finales proyectos como primer autor - ";
	                    		if(document.getElementById("txt_obs_5").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_5").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 28, 'Si', document.getElementById("txt_obs_5").value);
	                    	}else if(document.getElementById("ck_no_5").checked){
	                    		/*if(document.getElementById("txt_obs_5").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_5").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 28, 'No', document.getElementById("txt_obs_5").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_6").checked){
	                    		articulos += "- Informes técnicos finales proyectos como co-autor - ";
	                    		if(document.getElementById("txt_obs_6").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_6").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 29, 'Si', document.getElementById("txt_obs_6").value);
	                    	}else if(document.getElementById("ck_no_6").checked){
	                    		/*if(document.getElementById("txt_obs_6").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_6").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 29, 'No', document.getElementById("txt_obs_6").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_7").checked){
	                    		articulos += "- Libro nacional o internacional como editor o primer autor - ";
	                    		if(document.getElementById("txt_obs_7").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_7").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 30, 'Si', document.getElementById("txt_obs_7").value);
	                    	}else if(document.getElementById("ck_no_7").checked){
	                    		/*if(document.getElementById("txt_obs_7").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_7").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 30, 'No', document.getElementById("txt_obs_7").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_8").checked){
	                    		articulos += "- Libro nacional o internacionalcomo co-autor. O capítulo de libro nacional o internacional como primer autor. O cartillas, guías, manuales como autor - ";
	                    		if(document.getElementById("txt_obs_8").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_8").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 31, 'Si', document.getElementById("txt_obs_8").value);
	                    	}else if(document.getElementById("ck_no_8").checked){
	                    		/*if(document.getElementById("txt_obs_8").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_8").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 31, 'No', document.getElementById("txt_obs_8").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_9").checked){
	                    		articulos += "- Notas en revista - ";
	                    		if(document.getElementById("txt_obs_9").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_9").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 33, 'Si', document.getElementById("txt_obs_9").value);
	                    	}else if(document.getElementById("ck_no_9").checked){
	                    		/*if(document.getElementById("txt_obs_9").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_9").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 33, 'No', document.getElementById("txt_obs_9").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_10").checked){
	                    		articulos += "- Capítulo libro nacional o internacional como co-autor<br/>";
	                    		if(document.getElementById("txt_obs_10").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_10").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 32, 'Si', document.getElementById("txt_obs_10").value);
	                    	}else if(document.getElementById("ck_no_10").checked){
	                    		/*if(document.getElementById("txt_obs_10").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_10").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 32, 'No', document.getElementById("txt_obs_10").value);
	                    	}
	                    	
	                    	if(document.getElementById("ck_si_11").checked){
	                    		articulos += "- Resumen memorias en eventos<br/>";
	                    		if(document.getElementById("txt_obs_11").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_11").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 42, 'Si', document.getElementById("txt_obs_11").value);
	                    	}else if(document.getElementById("ck_no_11").checked){
	                    		/*if(document.getElementById("txt_obs_10").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_10").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 42, 'No', document.getElementById("txt_obs_11").value);
	                    	}
	                    	
	                	}else if(action=="p"){
	                		if(document.getElementById("ck_inves_si_1").checked){
	                    		articulos += "- Autor principal de propuesta de investigación aprobada - ";
	                    		if(document.getElementById("txt_obs_inves_1").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_1").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 34, 'Si', document.getElementById("txt_obs_inves_1").value);
	                    	}else if(document.getElementById("ck_inves_no_1").checked){
	                    		/*if(document.getElementById("txt_obs_inves_1").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_1").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 34, 'No', document.getElementById("txt_obs_inves_1").value);
	                    	}
	                		if(document.getElementById("ck_inves_si_2").checked){
	                    		articulos += "- Co-autor de propuesta de investigación aprobada - ";
	                    		if(document.getElementById("txt_obs_inves_2").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_2").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 35, 'Si', document.getElementById("txt_obs_inves_2").value);
	                    	}else if(document.getElementById("ck_inves_no_2").checked){
	                    		/*if(document.getElementById("txt_obs_inves_2").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_2").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 35, 'No', document.getElementById("txt_obs_inves_2").value);
	                    	}
	                		if(document.getElementById("ck_inves_si_3").checked){
	                    		articulos += "- Investigador principal en la ejecución de proyecto - ";
	                    		if(document.getElementById("txt_obs_inves_3").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_3").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 36, 'Si', document.getElementById("txt_obs_inves_3").value);
	                    	}else if(document.getElementById("ck_inves_no_3").checked){
	                    		/*if(document.getElementById("txt_obs_inves_3").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_3").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 36, 'No', document.getElementById("txt_obs_inves_3").value);
	                    	}	                		
	                		if(document.getElementById("ck_inves_si_4").checked){
	                    		articulos += "- Co-Investigador en la ejecución de proyecto - ";
	                    		if(document.getElementById("txt_obs_inves_4").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_4").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 37, 'Si', document.getElementById("txt_obs_inves_4").value);
	                    	}else if(document.getElementById("ck_inves_no_4").checked){
	                    		/*if(document.getElementById("txt_obs_inves_4").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_4").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 37, 'No', document.getElementById("txt_obs_inves_4").value);
	                    	}
	                		if(document.getElementById("ck_inves_si_5").checked){
	                    		articulos += "- Co-Investigador en la ejecución de proyecto - ";
	                    		if(document.getElementById("txt_obs_inves_5").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_5").value+".<br/>";
	                    		}
	                    		saveRevision(cc_revisor, cc_empleado, 38, 'Si', document.getElementById("txt_obs_inves_5").value);
	                    	}else if(document.getElementById("ck_inves_no_5").checked){
	                    		/*if(document.getElementById("txt_obs_inves_5").value==""){
	                    			articulos += "Sin observaciones<br/>";
	                    		}else{
	                    			articulos += "Observaciones: "+document.getElementById("txt_obs_inves_5").value+".<br/>";
	                    		}*/
	                    		saveRevision(cc_revisor, cc_empleado, 38, 'No', document.getElementById("txt_obs_inves_5").value);
	                    	}
	                		
	                		
	                	}  //Fin action
	            		
	            		
	            		msj = "Cordial saludo "+jefe+". <br/><br/>Le informo que el empleado "+empleado;
	            		
	            		
	            		if(articulos==""){
	            			msj += " no cumple con niguno de los requisitos.<br/><br/>";
	            		}else{
	            			msj += " cumple con los siguientes requisitos: <br/><br/>";	            			
	            			msj += articulos;
	            		}
	            		msj += "<br/><br/>Atentamente, <br/><br/>"+revisor+"<br/>";
	            		//alert("Llega antes de enviar el correo!");
	            		alert("Notificación enviada exitosamente!");
		            	 $.get("SendEmailJefe?host=" + "" + "&from=" + from + "&to=" + to + "&subject=" + subject + "&msj=" + msj, function (data) { 
		            		 
		                 });
	            	 });
            	});
            }
            
            function saveRevision(cc_revisor, cc_empleado, criterio, correcto, observaciones){
  				$.post('RevisarCriterios', {
                      cedula_revisor: cc_revisor,
                      cedula_empleado: cc_empleado,
                      id_criterio: criterio,
                      criterio_correcto: correcto, 
                      observaciones: observaciones
                  }, function (data){	
                                              
                  });
            }
            
            
            
        </script>
        
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top blue">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">menú</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand title" href="#">Sistema para reescalafonamiento</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="CerrarSession" id="cerrarsession"><i class="glyphicon glyphicon-log-in"></i> <span> Salir</span></a></li>
                        <!-- <li><a href="#">Ayúda</a></li> -->
                    </ul>

                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <img data-src="holder.js/140x140" class="img-circle center-block" alt="140x140" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNGU2ZGQ5NmIxZCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE0ZTZkZDk2YjFkIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ1LjUiIHk9Ijc0LjUiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true" style="width: 140px; height: 140px;">

                    <h4 class="text-center"><%=nombres%></h4>
                    <h4 class="text-center"><small><b id="labelPosition"></b><%=cargo%></small></h4>
                    <h5 class="text-center"><b id="labelSalaryScale"></b>Escalafon asignado</h5>
                    <h2 class="text-center"><b id="scalafonAlert1"></b></h2>
                    <h2 class="text-center">Puntaje:<br/><b id="scoreTotalAlert2"></b></h2>
                    <h4 class="text-center" style="display:none"><small><b>Programa: </b>Calidad Ambiental Marina</small></h4>


                    <br>

                    <ul class="nav nav-sidebar">
                    	<li id="iesa"><a href="#esa" onclick="ActiveElement('iesa')" >Escalaf&oacute;n actual</a></li>
                        <li id="iedu" class="active"><a  href="#edu" onclick="ActiveElement('iedu')"    >Estudios</a></li>
                        <li id="iexp" ><a  href="#exp" onclick="ActiveElement('iexp')">Experiencia toda la carrera profesional</a></li>
                        <li id="iproc"><a href="#proc" onclick="ActiveElement('iproc')" >Producci&oacute;n Cient&iacute;fica</a></li>
                        <li id="iecuc"><a href="#ecuc"  onclick="ActiveElement('iecuc')" >Elaboraci&oacute;n Conceptos &uacute;ltimos tres años</a></li>
                        <li id="iiei"><a href="#iei" onclick="ActiveElement('iiei')" >Investigaci&oacute;n e Innovaci&oacute;n</a></li>                        
                    </ul>

                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">                  
                     <!--<div class="alert alert-danger" style="display:none">
                        <strong>Informaci&oacute;n:</strong> La plataforma estar&aacute; disponible hasta el d&iacute;a 18 de agosto de 2015.
                    </div><br/>
                    <div class="alert alert-danger">
                        <strong>Informaci&oacute;n:</strong><span id="tintermitent">Cambie el nombre del archivo PDF con nombres cortos, sin espacios y sin tildes antes de subirlos. Por ejemplo:soporte.pdf</span> 
                    </div> -->

					 <div id="escalafonactual"> 
                        <h2 class="sub-header" >Escalaf&oacute;n actual</h2>

                        <div class="alert alert-warning"> 

                            <strong>Informaci&oacute;n:</strong>   Anexar en PDF certificaci&oacute;n emitida por TAL en el Invemar donde indique el escalaf&oacute;n actual y tiempo que ha permanecido en el mismo (podr&aacute; ser la misma certificaci&oacute;n de experiencia siempre y cuando se mencione en ella el escalaf&oacute;n)
                        </div>

                        <div class="table-responsive" id="esa">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Escalaf&oacute;n actual </th>
                                        <th id="agregar_escalafon">Agregar</th>
                                        <th>Soporte</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td id='tdscalafon' > 
                                            Experiencia en meses:<br/><br/>
                                            <input type="radio" name='tiempoexperiencia' id='t1' value='1'  /> 0 - 12 meses <br/>  
                                            <input type="radio" name='tiempoexperiencia' id='t2' value='2'/> 12 - 24 meses<br/>
                                            <input type="radio" name='tiempoexperiencia' id='t2' value='3'/> 24 meses en adelante <br/><br/>
                                            Seleccion el escalaf&oacute;n actual:<br/>
                                            <select id='escalafon' name='escalafon'>
                                                <!--  <option value='Investigador Auxiliar (II)'>Investigador Auxiliar</option>
                                                <option value='Investigador Asistente(II)'> Investigador Asistente</option>
                                                <option value='Investigador Adjunto(II)'>Investigador Adjunto</option>
                                                <option value='Investigador Asociado(II)'>Investigador Asociado</option>
                                                <option value='Investigador Titular(II)'>Investigador Titular</option>
                                                <option value='Investigador Senior'>Investigador Senior</option>-->
                                                <option value='1'>Investigador Auxiliar</option>
                                                <option value='2'> Investigador Asistente</option>
                                                <option value='3'>Investigador Adjunto</option>
                                                <option value='4'>Investigador Asociado</option>
                                                <option value='5'>Investigador Titular</option>
                                                <option value='6'>Investigador Senior</option>
                                            </select>

                                        </td>
                                        <td>
                                            <div id="agregar_ck_escalafon" class="checkbox"><label>
                                                    <input type="checkbox" value="" id='ck67' style="display:none">Seleccione esta opci&oacute;n
                                                </label>
                                            </div>
                                        </td>
                                        <td>

                                            <label for="67">Subir soporte: Anexar un &uacute;nico PDF con todos los soportes</label>
                                            <input type="file" name="67" id="67" alt="escalafon" style="display:none"><br/>
                                            <button type="button" id="btnd67" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(67)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link67"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message67" style="display:none">

                                            </div>
                                        </td>

                                    </tr>                                                                                   


                                </tbody>
                            </table>                        

                        </div>
                        <button type="button" class="score btn btn-info btn-lg"  id="ESCALAFON" >Calificar Escalafon Actual</button>
                        <span id="totalscore_ESCALAFON" > </span>
                    </div>

                    <div id="seccionStudios">
                        <h2 class="sub-header" id="edu">Estudios</h2>

                        <div class="alert alert-warning">
                            <strong>Informaci&oacute;n:</strong> Anexar en PDF diploma de cada t&iacute;tulo que se marque.  Si es t&iacute;tulo internacional deber&aacute; estar convalidado u homologado en el pa&iacute;s y se debe anexar el soporte respectivo
                        </div>

                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Titulo obtenido</th>
                                        <th id="agregar_estudios">Agregar</th>
                                        <th>Soporte</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Pregrado(s)</td>
                                        <td>
                                            <div id="agregar_ck_est_1" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck38">Seleccione esta opci&oacute;n
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="38">Subir soporte: Organice en un &uacute;nico PDF todos los pregrados que tiene</label>
                                            <input type="file" name="38" id="38" alt="Pregrado" />                                       
                                            <button type="button"  id="btnd38" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary"  onclick="deleteCritherium(38)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp;
                                            <a  id="link38"  href="" style="display:none" target="_blank">Ver archivo cargado</a></button>
                                            <div class="alert alert-danger" id="message38" style="display:none" >
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Especialización(s)</td>
                                        <td><div id="agregar_ck_est_2" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck39">Seleccione esta opción
                                                </label>
                                            </div></td>
                                        <td>
                                            <label for="39">Subir soporte: Organice en un &uacute;nico PDF todas las especializaciones que tiene</label>
                                            <input type="file" name="39" id="39" alt="Especializacion"><br/>                                       
                                            <button type="button" id="btnd39" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(39)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link39"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message39" style="display:none">

                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Maestría(s)</td>
                                        <td><div id="agregar_ck_est_3" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck40" >Seleccione esta opción
                                                </label>
                                            </div></td>
                                        <td>

                                            <label for="40">Subir soporte: Organice en un &uacute;nico PDF todos maestr&iacute;as que tiene</label>
                                            <input type="file" name="40" id="40" alt="Maestria"><br/>
                                            <button type="button" id="btnd40" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(40)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link40"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message40" style="display:none">

                                            </div>


                                        </td>

                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>Doctorado(s)</td>
                                        <td><div id="agregar_ck_est_4" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck41">Seleccione esta opción
                                                </label>
                                            </div></td>
                                        <td>

                                            <label for="41">Subir soporte: Organice en un &uacute;nico  PDF todos los doctorados que tiene</label>
                                            <input type="file" name="41" id="41" alt="Doctorado"><br/>
                                            <button type="button" id="btnd41" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(41)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link41"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message41" style="display:none">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td>Postdoctorado(s)</td>
                                        <td><div id="agregar_ck_est_4" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck71">Seleccione esta opción
                                                </label>
                                            </div></td>
                                        <td>

                                            <label for="71">Subir soporte: Organice en un &uacute;nico  PDF todos los doctorados que tiene</label>
                                            <input type="file" name="71" id="71" alt="PostDoctorado"><br/>
                                            <button type="button" id="btnd71" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(71)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link71"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message71" style="display:none">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <button type="button" class="score btn btn-info btn-lg" data-puntmax="10" id="ESTUDIOS" >Calificar Estudios</button>
                        <span id="totalscore_ESTUDIOS" > </span>
                    </div>

					<div id="seccionExperiencia">
	                    <h2 class="sub-header" id="exp">Experiencia toda la carrera profesional</h2>
	
	                    <div class="alert alert-warning">
	                        <strong>Informaci&oacute;n:</strong> Anexar certificaciones laborales con fechas exactas de las contrataciones que demuestran el tiempo de experiencia durante toda su carrera profesional. 
	                    </div>
	                    <div class="table-responsive">
	
	                        <table class="table table-bordered">
	                            <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Experiencia</th>
	                                    <th id="agregar_exp">Agregar</th>
	                                    <th>Soporte</th>
	
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <th scope="row">1</th>
	                                    <td>Post-título profesional</td>
	                                    <td>
	                                        <div id="agregar_ck_exp_1" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck43">Seleccione esta opción
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td>
	                                        <label for="43">Subir soporte: Organice en un &uacute;nico PDF todos soportes</label>
	                                        <input type="file" name="43" id="43" alt="postprofesional" />                                        
	                                        <button type="button"  id="btnd43" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary"  onclick="deleteCritherium(43)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp;
	                                        <a  id="link43"  href="" style="display:none" target="_blank">Ver archivo cargado</a><button type="button" class="close" aria-label="Close" onclick="deleteCritherium(43)"></button>
	                                        <div class="alert alert-danger" id="message43" style="display:none" >
	
	                                        </div>
	                                    </td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">2</th>
	                                    <td>Post-t&iacute;tulo Especializaci&oacute;n</td>
	                                    <td><div id="agregar_ck_exp_2" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck44">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="44">Subir soporte: Organice en un &uacute;nico PDF todos soportes</label>
	                                        <input type="file" name="44" id="44" alt="postespecializacion"><br/>
	                                        <button type="button" id="btnd44" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(44)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link44"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message44" style="display:none">
	
	                                        </div>
	                                    </td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">3</th>
	                                    <td>Post-t&iacute;tulo Maestr&iacute;a</td>
	                                    <td><div id="agregar_ck_exp_3" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck45">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="45">Subir soporte: Organice en un &uacute;nico PDF todos soportes</label>
	                                        <input type="file" name="45" id="45" alt="postmaestria"><br/>
	                                        <button type="button" id="btnd45" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(45)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link45"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message45" style="display:none">
	
	                                        </div>                         
	
	                                    </td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">4</th>
	                                    <td>Post-t&iacute;tulo Doctorado</td>
	                                    <td><div id="agregar_ck_exp_4" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck46">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="46">Subir soporte:Organice en un &uacute;nico PDF todos soportes</label>
	                                        <input type="file" name="46" id="46" alt="postdoctorado"><br/>
	                                        <button type="button" id="btnd46" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(46)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link46"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message46" style="display:none">
	
	                                        </div>       
	
	
	                                    </td>
	
	                                </tr>
	
	                            </tbody>
	                        </table>
	                          <button type="button" class="score btn btn-info btn-lg"  id="EXPERIENCIA_LABORAL" >Calificar Experiencia</button>
	                        <span id="totalscore_EXPERIENCIA_LABORAL" > </span>
	                    </div>
					</div>

                    <!-- ********************************************* -->
					<div id="seccionProduccion">
	                    <h2 class="sub-header" id="proc">Producci&oacute;n Cient&iacute;fica</h2>
	
	                    <div class="alert alert-warning">
	                        <strong>Informaci&oacute;n:</strong>Tener encuenta las indicaciones para  cada criterio. 
	                    </div>
	
	                    <div class="table-responsive">
	
	                        <table class="table table-bordered">
	                            <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Tipo de producci&oacute;n</th>
	                                    <th id="agregar_prod">Agregar</th>
	                                    <th>Soporte</th>										
										<th id="correcto_prod">¿Está correcto?</th>
										<th id="observaciones_prod">Observaciones</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <th scope="row">1</th>
	                                    <td>Art&iacute;culo cient&iacute;fico en revista internacional indexada como primer autor</td>
	                                    <td>
	                                        <div id="agregar_ck_prod_1" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck47">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td>
	                                        <label for="47">Subir soporte: Anexar art&iacute;culos(s) en un &uacute;nico PDF. Colocar al menos la car&aacute;tula o primera p&aacute;gina del art&iacute;culo o DOI </label>
	                                        <input type="file" name="47" id="47" alt="articulocientifico"><br/>
	                                        <button type="button" id="btnd47" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(47)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link47"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message47" style="display:none">	
	                                        </div>    	
	                                    </td>
	                                    <td id="correcto_arti_1">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_1">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_1">No
												</label>
											</div>
										</td>
										<td id="observacion_1">	
											<textarea id="txt_obs_1" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	                                </tr>
	                                
	                                <tr>
	                                    <th scope="row">2</th>
	                                    <td>Art&iacute;culo cient&iacute;fico en revista internacional indexada como co-autor</td>
	                                    <td><div id="agregar_ck_prod_2" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck64">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td>
	                                        <label for="64">Subir soporte: Anexar art&iacute;culo(s) en un &uacute;nico PDF. Colocar al menos la car&aacute;tula o primera p&aacute;gina del art&iacute;culo o DOI</label>
	                                        <input type="file" name="64" id="64" alt="articulocientifico"><br/>
	                                        <button type="button" id="btnd64" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(64)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link64"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message64" style="display:none">
	
	                                        </div>    
	
	
	                                    </td>
	                                    
	                                    <td id="correcto_arti_2">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_2">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_2">No
												</label>
											</div>
										</td>
										
										<td id="observacion_2">	
											<textarea id="txt_obs_2" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	                                </tr> 
	                                <tr>
	                                    <th scope="row">3</th>
	                                    <td>Art&iacute;culo cient&iacute;fico en revista nacional  indexada como primer autor</td>
	                                    <td><div id="agregar_ck_prod_3" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck63">Seleccione esta opción
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="63">Subir soporte: Anexar art&iacute;culo(s) en un &uacute;nico PDF. Colocar al menos la car&aacute;tula o primera p&aacute;gina del art&iacute;culo o DOI</label>
	                                        <input type="file" name="63" id="63" alt="articulocientifico"><br/>
	                                        <button type="button" id="btnd63" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(63)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link63"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message63" style="display:none">
	
	                                        </div> 
	
	                                    </td>
	                                    
	                                    <td id="correcto_arti_3">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_3">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_3">No
												</label>
											</div>
										</td>
										
										<td id="observacion_3">	
											<textarea id="txt_obs_3" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	                                </tr>
	                                <tr>
	                                    <th scope="row">4</th>
	                                    <td>Art&iacute;culo cient&iacute;fico en revista nacional indexada como co-autor. </td>
	                                    <td><div id="agregar_ck_prod_4" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck48">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>               
	
	                                        <label for="48">Subir soporte: Anexar art&iacute;culo(s) en un &uacute;nico PDF. Colocar al menos la car&aacute;tula o primera p&aacute;gina del art&iacute;culo o DOI</label>
	                                        <input type="file" name="48" id="48" alt="articulocientifico"><br/>
	                                        <button type="button" id="btnd48" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(48)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link48"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message48" style="display:none">
	
	                                        </div> 
	                                    </td>
	                                    
	                                    <td id="correcto_arti_4">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_4">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_4">No
												</label>
											</div>
										</td>
										
										<td id="observacion_4">	
											<textarea id="txt_obs_4" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	
	
	                                <tr>
	                                    <th scope="row">5</th>
	                                    <td>Informes t&eacute;cnicos finales proyectos  como primer autor</td>
	                                    <td><div id="agregar_ck_prod_5" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck53">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="53">Subir soporte: Anexar PDF de la portada y p&aacute;gina institucional donde aparecen los autores, el año y la cita del informe de los &uacute;ltimos tres años  Debe estar certificado por el coordinador de programa</label>
	                                        <input type="file" name="53" id="53" alt="informtecnico"><br/>
	                                        <button type="button" id="btnd53" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(53)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link53"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message53" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_5">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_5">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_5">No
												</label>
											</div>
										</td>
										
										<td id="observacion_5">	
											<textarea id="txt_obs_5" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	
	                                <tr>
	                                    <th scope="row">6</th>
	                                    <td>Informes t&eacute;cnicos finales proyectos  como co-autor</td>
	                                    <td><div id="agregar_ck_prod_6" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck65">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="65">Subir soporte: Anexar PDF de la portada y p&aacute;gina institucional donde aparecen los autores, el año y la cita del informe de los &uacute;ltimos tres años. Debe estar certificado por la entidad</label>
	                                        <input type="file" name="65" id="65" alt="informtecnico"><br/>
	                                        <button type="button" id="btnd65" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(65)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link65"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message65" style="display:none">
	
	                                        </div>
	
	                                    </td>
	                                    
	                                    <td id="correcto_arti_6">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_6">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_6">No
												</label>
											</div>
										</td>
										
										<td id="observacion_6">	
											<textarea id="txt_obs_6" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">7</th>
	                                    <td>Libro nacional o internacional como editor o primer autor.</td>
	                                    <td ><div id="agregar_ck_prod_7" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck50">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td> 
	                                        <label for="50">Subir soporte: Anexar en PDF Portada/contrapartada del libro de los &uacute;ltimos tres años, donde aparezca su nombre como editor</label>
	                                        <input type="file" name="50" id="50" alt="libro"><br/>
	                                        <button type="button" id="btnd50" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(50)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link50"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message50" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_7">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_7">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_7">No
												</label>
											</div>
										</td>
										
										<td id="observacion_7">	
											<textarea id="txt_obs_7" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>                       
	                                <tr>
	                                    <th scope="row">8</th>
	                                    <td>Libro nacional o internacionalcomo co-autor. O capítulo de libro nacional o internacional como primer autor. O cartillas, guías, manuales como autor	 
	                                    </td>
	                                    <td><div id="agregar_ck_prod_8" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck51" >Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	
	                                        <label for="51">Subir soporte: Anexar en PDF Portada/contrapartada del libro de los &uacute;ltimos tres años, donde aparezca su nombre como co-autor</label>
	                                        <input type="file" name="51" id="51" alt="libro"><br/>
	                                        <button type="button" id="btnd51" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(51)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link51"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message51" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_8">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_8">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_8">No
												</label>
											</div>
										</td>
										
										<td id="observacion_8">	
											<textarea id="txt_obs_8" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">9</th>
	                                    <td>Notas en revista</td>
	                                    <td><div id="agregar_ck_prod_9" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck49">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="49">Subir soporte: Anexar nota(s) en PDF</label>
	                                        <input type="file" name="49" id="49" alt="nota"><br/>
	                                        <button type="button" id="btnd49" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(49)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link49"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message49" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_9">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_9">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_9">No
												</label>
											</div>
										</td>
										
										<td id="observacion_9">	
											<textarea id="txt_obs_9" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">10</th>
	                                    <td>Capítulo libro nacional o internacional como co-autor</td>
	                                    <td><div id="agregar_ck_prod_10" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck52">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="52">Subir soporte: Anexar PDF del resumen y portada de la publicaci&oacute;n de los &uacute;ltimos tres años, donde aparece el mismo</label>
	                                        <input type="file" name="52" id="52" alt="nota"><br/>
	                                        <button type="button" id="btnd52" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(52)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link52"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message52" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_10">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_10">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_10">No
												</label>
											</div>
										</td>
										
										<td id="observacion_10">	
											<textarea id="txt_obs_10" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                
	                                <tr>
	                                    <th scope="row">11</th>
	                                    <td>Resumen memorias en eventos</td>
	                                    <td><div id="agregar_ck_prod_11" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck70">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="70">Subir soporte: Anexar resumen en PDF </label>
	                                        <input type="file" name="70" id="70" alt="nota"><br/>
	                                        <button type="button" id="btnd70" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(70)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link70"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message70" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_arti_11">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_si_11">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_no_11">No
												</label>
											</div>
										</td>
										
										<td id="observacion_11">	
											<textarea id="txt_obs_11" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                
	
	                            </tbody>
	                        </table>
	
	                    </div>
                    </div>
										
                    <button type="button" class="scoreProd btn btn-info btn-lg" data-puntmax="30"  id="PRODUCCION_CIENTIFICA" >Calificar Producci&oacute;n Cient&iacute;fica</button>
                    <button type="button" onclick="sendEmailJefe(host, from, to, subject, msj)" id="VERIFICACION_PRODUCCION_CIENTIFICA" >Enviar notificaci&oacute;n</button>
                    

                    <!-- ********************************************* -->
                    <div id="seccionConceptosTecnicos">
                        <h2 class="sub-header" id="ecuc">Elaboraci&oacute;n Conceptos &uacute;ltimos tres años</h2>

                        <div class="alert alert-warning">
                            <strong>Informaci&oacute;n:</strong>Tener encuenta las indicaciones para  cada criterio.  
                        </div>
                        <div class="alert info-warning">
                            <ul>
                                <!-- <li><a target="_blank" id="l2012" style="cursor:pointer" >Listado de Conceptos 2012</a></li>
                                <li><a target="_blank" id="l2013" style="cursor:pointer">Listado de Conceptos 2013</a></li>
                                <li><a target="_blank" id="l2014" style="cursor:pointer">Listado de Conceptos 2014</a></li> 
                                <li><a target="_blank" id="l2015" style="cursor:pointer">Listado de Conceptos 2015</a></li>
                                <li><a target="_blank" id="l2016" style="cursor:pointer">Listado de Conceptos 2016</a></li>
                                <li><a target="_blank" id="l2017" style="cursor:pointer">Listado de Conceptos 2017</a></li>
                                <li><a target="_blank" id="l2018" style="cursor:pointer">Listado de Conceptos 2018</a></li>-->
                                <li><a target="_blank" id="l2014-2019" style="cursor:pointer">Listado de Conceptos 2014 al 2019</a></li>
                            </ul>
                        </div>


                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Criterio</th>
                                        <th id="agregar_cpt">Agregar</th>
                                        <th>Soporte</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>L&iacute;der CPT (Concepto t&eacute;cnico) C y D</td>
                                        <td>
                                            <div id="agregar_ck_cpt_1" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck54">Seleccione esta opci&oacute;n
                                                </label>
                                            </div>
                                        </td>
                                        <td>

                                            <label for="54">Subir soporte: Anexar PDF portada y p&aacute;gina institucional donde aparece como primer autor/l&iacute;der y el tipo de Concepto</label>
                                            <input type="file" name="54" id="54" alt="nota"><br/>
                                            <button type="button" id="btnd54" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(54)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link54"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message54" style="display:none">

                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Líder CPT (Concepto t&eacute;cnico) A y B</td>
                                        <td><div id="agregar_ck_cpt_2" class="checkbox"><label>
                                                    <input type="checkbox" value="" id="ck55">Seleccione esta opción
                                                </label>
                                            </div></td>
                                        <td>

                                            <label for="55">Subir soporte: Anexar PDF portada y p&aacute;gina institucional donde aparece como primer autor/l&iacute;der y el tipo de Concepto</label>
                                            <input type="file" name="55" id="55" alt="nota"><br/>
                                            <button type="button" id="btnd55" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(55)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link55"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message55" style="display:none">

                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Co-autores CPTs (Conceptos t&eacute;cnicos)</td>
                                        <td><div id="agregar_ck_cpt_3" class="checkbox"><label>
                                                    <input  type="checkbox" value="" id="ck56">Seleccione esta opci&oacute;n
                                                </label>
                                            </div></td>
                                        <td>

                                            <label for="55">Subir soporte: Anexar PDF portada y p&aacute;gina institucional donde aparece como co-autor y el tipo de Concepto</label>
                                            <input type="file" name="56" id="56" alt="nota"><br/>
                                            <button type="button" id="btnd56" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(56)">
                                                <span class='glyphicon glyphicon-trash'></span> Borrar 
                                            </button>&nbsp; <a  id="link56"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
                                            <div class="alert alert-danger" id="message56" style="display:none">

                                            </div>


                                        </td>

                                    </tr>

                                </tbody>
                            </table>

                        </div>
                        <button type="button" class="score btn btn-info btn-lg" data-puntmax="10"  id="CONCEPTOS_TECNICOS" >Calificar Elaboraci&oacute;n de Conceptos T&eacute;cnicos</button>
                    </div>
                    <!-- ********************************************* -->
					<div id=seccionInnovacion>
	                    <h2 class="sub-header" id="iei">Investigaci&oacute;n e Innovaci&oacute;n &uacute;ltimos tres años</h2>
	
	                    <div class="alert alert-warning">
	                        <strong>Informaci&oacute;n:</strong> Aregar los c&oacute;digos SPI en un PDF para cada caso que corresponda.
	                    </div>
	
	                    <div class="table-responsive">
	
	                        <table class="table table-bordered  table-fixed">
	                            <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Criterio</th>
	                                    <th id="agregar_inves">Agregar</th>
	                                    <th>Soporte</th>
	                                    <th id="correcto_inves">¿Está correcto?</th>
										<th id="observaciones_inves">Observaciones</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <th scope="row">1</th>
	                                    <td>Autor principal de propuesta de investigaci&oacuten aprobada</td>
	                                    <td>
	                                        <div id="agregar_ck_inves_1" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck57">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td>
	                                        <label for="57">Subir soporte: Organice todos los soportes en un &uacute;nico PDF</label>
	                                        <input type="file" name="57" id="57" alt="investigacion"><br/>
	                                        <button type="button" id="btnd57" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(57)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link57"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message57" style="display:none">
	
	                                        </div>
	
	                                    </td>
	                                    
	                                    <td id="correcto_inves_1">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_1">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_1">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_1">	
											<textarea id="txt_obs_inves_1" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">2</th>
	                                    <td>Co-autor de propuesta de investigación aprobada</td>
	                                    <td><div id="agregar_ck_inves_2" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck58">Seleccione esta opción
	                                            </label>
	                                        </div></td>
	                                    <td>
	
	                                        <label for="58">Subir soporte: Organice todos los soportes en un &uacute;nico PDF</label>
	                                        <input type="file" name="58" id="58" alt="investigacion"><br/>
	                                        <button type="button" id="btnd58" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(58)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link58"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message58" style="display:none">
	
	                                        </div>
	
	                                    </td>
	                                    
	                                    <td id="correcto_inves_2">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_2">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_2">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_2">	
											<textarea id="txt_obs_inves_2" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">3</th>
	                                    <td>Investigador principal en la ejecuci&oacute;n de proyecto</td>
	                                    <td><div id="agregar_ck_inves_3" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck59">Seleccione esta opción
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="59">Subir soporte: Organice todos los soportes en un &uacute;nico PDF</label>
	                                        <input type="file" name="59" id="59" alt="investigacion"><br/>
	                                        <button type="button" id="btnd59" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(59)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link59"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message59" style="display:none">
	
	                                        </div>
	                                    </td>
	                                    
	                                    <td id="correcto_inves_3">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_3">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_3">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_3">	
											<textarea id="txt_obs_inves_3" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">4</th>
	                                    <td>Co-Investigador en la ejecuci&oacute;n de proyecto</td>
	                                    <td><div id="agregar_ck_inves_4" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck60">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="60">Subir soporte: Organice todos los soportes en un &uacute;nico PDF</label>
	                                        <input type="file" name="60" id="60" alt="investigacion"><br/>
	                                        <button type="button" id="btnd60" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(60)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link60"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message60" style="display:none">
	
	                                        </div>
	
	                                    </td>
	                                    
	                                    <td id="correcto_inves_4">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_4">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_4">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_4">	
											<textarea id="txt_obs_inves_4" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <tr>
	                                    <th scope="row">5</th>
	                                    <td>Participaci&oacute;n en programaci&oacute;n de  software y/o aplicativos, paquetes tecnol&oacute;gicos, registros y patentes</td>
	                                    <td><div id="agregar_ck_inves_5" class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck61">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="61">Subir soporte: Anexar en un &uacute;nico PDF todos los soportes</label>
	                                        <input type="file" name="61" id="61" alt="investigacion"><br/>
	                                        <button type="button" id="btnd61" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(61)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link61"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message61" style="display:none">
	
	                                        </div>
	
	                                    </td>
	                                    
	                                    <td id="correcto_inves_5">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_5">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_5">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_5">	
											<textarea id="txt_obs_inves_5" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	
	                                </tr>
	                                <!-- <tr>
	                                    <th scope="row">6</th>
	                                    <td>Co-autor en la programaci&oacute;n de  software y/o aplicativos, paquetes tecnol&oacute;gicos,registros y patentes</td>
	
	                                    <td><div class="checkbox"><label>
	                                                <input type="checkbox" value="" id="ck62">Seleccione esta opci&oacute;n
	                                            </label>
	                                        </div></td>
	                                    <td>
	                                        <label for="62">Subir soporte: Anexar en un &uacute;nico PDF todos los soportes</label>
	                                        <input type="file" name="62" id="62" alt="investigacion"><br/>
	                                        <button type="button" id="btnd62" style="display:none" data-deleting-text="Deleting..." class="btn btn-primary" onclick="deleteCritherium(62)">
	                                            <span class='glyphicon glyphicon-trash'></span> Borrar 
	                                        </button>&nbsp; <a  id="link62"  href="" style="display:none" target="_blank">Ver archivo cargado</a>
	                                        <div class="alert alert-danger" id="message62" style="display:none">
	
	                                        </div>	
	                                    </td>
	                                    
	                                    <td id="correcto_inves_6">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="Si" id="ck_inves_si_6">Si
												</label>
												<label>
													<input type="checkbox" value="No" id="ck_inves_no_6">No
												</label>
											</div>
										</td>
										<td id="observacion_inves_6">	
											<textarea id="txt_obs_inves_6" rows="4" cols="50" style="margin: 0px; height: 87px; width:100%;"></textarea>										
										</td>
	                                </tr> -->
	                            </tbody>
	                        </table>
                    </div>
                    <button type="button" class="scoreInves btn btn-info btn-lg" data-puntmax="30"  id="INVESTIGACION_INNOVACION" >Calificar Investigaci&oacute;n e Innovaci&oacute;n</button>
                    <button type="button" onclick="sendEmailJefe(host, from, to, subject, msj)" id="VERIFICACION_INVESTIGACION_INNOVACION" >Enviar notificaci&oacute;n</button>
                    <br><br><br>
                    
                    
					</div>
                    <!-- ********************************************* -->
                   
                   <div id="MANEJO_DATOS">
	                   <p align="justify">Teniendo en cuenta la normatividad vigente en Colombia respecto a la protección, tratamiento y manejo de datos personales, Ley 1581 de 2012 
	                   y sus Decretos Reglamentarios, INVEMAR pone a su consideración la presente información con el fin de solicitar su autorización respecto a los 
	                   datos y soportes que usted está suministrando. Los datos personales y soportes suministrados por usted como titular serán utilizados por INVEMAR 
	                   para fines relacionados con la información de nuestro instituto.
	                   <br><br>Autorizo:</p>
	                   
	                   <div class="checkbox">
							<label>
								<input type="checkbox" value="Si" id="ck_autorizo">Si
							</label>
							<label>
								<input type="checkbox" value="No" id="ck_no_autorizo">No
							</label>
					   </div>
                   </div>                   
                   
                   
                   <br>
                   
                   <button type="button" class="habilitarUsuario btn btn-info btn-lg"  id="HABILITAR_INVESTIGADOR" >Habilitar carga de soportes</button>
                   
                    <button type="button"  id="finishprocess" style="" data-deleting-text="Deleting..." class="btn btn-primary"   >
                        <span class=''></span> Finalizar el proceso y someter hoja de vida
                    </button>
                    
                    <footer>
                        <div class="row">
                            <div class="col-md-1 col-sm-3">
                                <img src="images/logo-web.png" style="height:90px" alt="Invemar">
                            </div>
                            <div class="col-md-11 col-sm-8">
                                <address>
                                    <strong>Invemar</strong><br>
                                    Calle 25 No 2-55 Playa salguero<br>
                                    Santa Marta - Colombia<br>
                                    <abbr title="Phone">P:</abbr> +57 (5) 432-8600
                                </address>
                                <p class="text-right"><small>Powered by: LabSIS and CMC</small></p>
                            </div>
                        </div>
                    </footer>

                </div>
            </div>
        </div>


		
        <div  id="mymodal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    Cargando archivo...
                </div>
            </div>
        </div>


        <div id="modalScoring" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Asignar el puntaje </h4><h5 id='texto_cantidad' style="display:none">Ingrese primero la cantidad de soportes en la fila del criterio y luego marque el criterio haciendo clic en el recuadro izquierdo</h5>
                        <h3 class="modal-title" style="color:red;display:none"  >Click sobre el criterio que desea aplicar.</h3>
                    </div>
                    <div class="modal-body">
                        <table id="critheriumTable" data-search="true"                              
                               data-page-size="4"                                                                                 
                               data-minimum-count-columns="2"
                               data-show-pagination-switch="true"                                                                      
                               data-show-footer="true"                          
                               >
                             </table>
                    </div>
                    <div><span style="font-weight:bold;margin-left: 20px">Sub Total:</span><span style="margin-left: 15px;font-size: 14px" id="resultTotal"></span></div>
                    <!--  <div><span style="font-weight:bold;margin-left: 20px">Puntaje Máximo de la sección:</span><span style="margin-left: 15px;font-size: 14px" id="puntajeMaximo"></span></div>
                    --><div><span style="font-weight:bold;margin-left: 20px">Total:</span><span style="margin-left: 15px;font-size: 14px" id="scoreTotal"></span></div>
                    <div> <button type="button"  id='showSalaryScale' class="btn btn-info btn-lg"  style="margin-left: 20px;margin-top:5px;font-weight: bold">Ver escalaf&oacute;n</button></div>
                    <h5 class="text-center"><b id="labelSalaryScale2"></b>Escalafon asignado:</h5>
                    <h2 class="text-center"><b id="scalafonAlert2"></b></h2>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="font-weight: bold">Cerrar</button>
                    </div>
                </div>

            </div>
        </div>            

        <div id="modalmessage" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Sistema de Escalafonamiento</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>

            </div>
        </div>            

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <!-- Latest compiled and minified CSS -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>

    </body>
</html>