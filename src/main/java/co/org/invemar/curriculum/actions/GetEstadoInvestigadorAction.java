package co.org.invemar.curriculum.actions;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.curriculum.models.ActivacionInvestigador;
import co.org.invemar.curriculum.models.Preseleccionated;

@WebServlet(name = "GetEstadoInvestigadorAction", urlPatterns = {"/GetEstadoInvestigador"})
public class GetEstadoInvestigadorAction extends HttpServlet {
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            JSONObject jsonObject = new JSONObject();

            response.setContentType("text/json; charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            Preseleccionated preseleccionated = new Preseleccionated();
            String cedula = request.getParameter("cedula");

            ActivacionInvestigador activacionInvestigador = new ActivacionInvestigador();
            
            if (cedula != null) {
                response.getWriter().write(activacionInvestigador.getEstadoInvestigador(cedula).toString());
            } else {
                response.getWriter().write(jsonObject.put("state", "logged").toString());
            }
        } catch (JSONException ex) {
            Logger.getLogger(GetInfoRevisorAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
