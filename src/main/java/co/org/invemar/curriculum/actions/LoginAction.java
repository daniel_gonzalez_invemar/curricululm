/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Empleados;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
@WebServlet("/Login")
public class LoginAction extends HttpServlet {

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject jsonObject = new JSONObject();
        String cedula = request.getParameter("cedula");
        String pwd = request.getParameter("pwd");
        HttpSession hs =null;
        
        Empleados e = new Empleados();      
     
        try {
            
            JSONObject json= e.loginEmpleados(cedula, pwd);
            
                        
            
            if (cedula.equalsIgnoreCase(json.getString("cedula")) && pwd.equalsIgnoreCase(json.getString("clave"))) {                
                
               
                
                hs= request.getSession(true);
                hs.setAttribute("cedula", cedula);
                hs.setAttribute("cargo", json.getString("cargo"));
                hs.setAttribute("nombres", json.getString("nombre")+" "+json.getString("apellido"));
                hs.setAttribute("tipo_personal", json.getString("tipo"));
                hs.setAttribute("revisor_curriculum", json.getString("revisor_curriculum"));
              
                
                if ("S".equalsIgnoreCase(json.getString("revisor_curriculum"))) {
                    jsonObject.put("state", "logged");   
                    jsonObject.put("path", "list.jsp");
                }else if ("C".equalsIgnoreCase(json.getString("revisor_curriculum"))) {//USUARIO REVISOR DE CDO
                    jsonObject.put("state", "logged");   
                    jsonObject.put("path", "list.jsp");
                }else if ("P".equalsIgnoreCase(json.getString("revisor_curriculum"))) {//USUARIO REVISOR DE PLA
                    jsonObject.put("state", "logged");   
                    jsonObject.put("path", "list.jsp");
                }else if ("T".equalsIgnoreCase(json.getString("revisor_curriculum"))) {//USUARIO REVISOR DE TAL
                    jsonObject.put("state", "logged");   
                    jsonObject.put("path", "list.jsp");
                }
                else{
                     if ("S".equalsIgnoreCase(json.getString("activo"))){
                        jsonObject.put("state", "logged");   
                        jsonObject.put("path", "subir.jsp?cedula="+cedula);
                     }
                    
                }
                
            } else {               
                jsonObject.put("state", "notlogged");
                
            }
           
        } catch (JSONException ex) {
            Logger.getLogger(LoginAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());
      
    }
}
