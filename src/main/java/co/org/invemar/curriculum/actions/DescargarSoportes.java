package co.org.invemar.curriculum.actions;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.enterprise.context.spi.Context;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




@WebServlet("/descarga-soporte")
public class DescargarSoportes extends HttpServlet {
	public static final String CONTENT_TYPE = "application/pdf";
	//public static final String RUTA = File.separator+".."+File.separator+"uploads"+File.separator;
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String root = getServletContext().getRealPath("/");
        File ruta = new File(root + "/../../uploads");
		
        response.setContentType(CONTENT_TYPE);
        String nombre= request.getParameter("nombre");
        OutputStream os = response.getOutputStream();

        try {
            File my_file = new File(ruta+File.separator+nombre);
            FileInputStream in = new FileInputStream(my_file);                
            int b;
            byte[] buffer = new byte[10240];
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);
            }
            os.close();
            
        } catch (Exception e) {
           e.printStackTrace(); //System.out.println(e);
        } 
    }

}
