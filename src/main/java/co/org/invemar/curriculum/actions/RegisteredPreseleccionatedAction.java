/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.EstadosUsuariosEscalafon;
import co.org.invemar.curriculum.models.Preseleccionated;
import co.org.invemar.curriculum.models.UsuariosCriteriosEscalafon;
import co.org.invemar.curriculum.pojos.AttributePreseleccionated;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
@WebServlet("/RegisteredPreseleccionated")
public class RegisteredPreseleccionatedAction extends HttpServlet {
     /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject jsonObject = new JSONObject();
        HttpSession session = request.getSession(false);
        
        
        String cedula_ = (String) session.getAttribute("cedula");
        
        String cedula      = request.getParameter("identification");
        String name        = request.getParameter("name");
        String lastName    = request.getParameter("lastName");
        String job         = request.getParameter("job");
        String clave         = request.getParameter("clave");
        String active         = request.getParameter("active");
       
        try {
            if (cedula_ != null) {                
                
                Preseleccionated preseleccionated = new Preseleccionated();                 
                                               
                AttributePreseleccionated attributePreseleccionated = new  AttributePreseleccionated();
                
                attributePreseleccionated.setIdentification(cedula);
                attributePreseleccionated.setName(name);
                attributePreseleccionated.setLastName(lastName);
                attributePreseleccionated.setJob(job);
                attributePreseleccionated.setActive(active);
                attributePreseleccionated.setPassword(clave);
                
                
                ResultadoTransaccion resultadoTransaccion = preseleccionated.register(attributePreseleccionated);
                jsonObject.put("result", resultadoTransaccion.getCodigo());
                jsonObject.put("state", "logged");
                jsonObject.put("message", resultadoTransaccion.getMensaje());
                
                

            } else {
                jsonObject.put("state", "notlogged");
            }
        } catch (JSONException ex) {
            Logger.getLogger(RegisteredPreseleccionatedAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());

    }
}
