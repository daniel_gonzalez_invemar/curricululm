/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Empleados;
import co.org.invemar.curriculum.models.UsuariosCriteriosEscalafon;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
@WebServlet("/ListCritheriumFile")
public class FilePathCritheriumAction extends HttpServlet {

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject jsonObject = new JSONObject();
        String cedula = request.getParameter("cedula");     
        
        String root = getServletContext().getRealPath("/");
        
       UsuariosCriteriosEscalafon uce = new UsuariosCriteriosEscalafon();
       String data= uce.getCriteriosConArchivosPorCedula(cedula, root);   
        

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.addHeader( "X-XSS-Protection", "1; mode=block" ); 
        response.addHeader( "X-Content-Type-Options", "nosniff" );        
        response.getWriter().write(data);
      
    }
    
}
