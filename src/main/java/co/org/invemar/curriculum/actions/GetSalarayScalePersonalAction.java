/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Empleados;
import co.org.invemar.curriculum.models.EstadosUsuariosEscalafon;
import co.org.invemar.curriculum.models.Personal;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
@WebServlet("/getsalaryscalepersonal")
public class GetSalarayScalePersonalAction extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String cedula= request.getParameter("cedula");
        JSONObject jsonObject = new JSONObject();
        
        Personal personal = new Empleados();
        
       JSONObject jsonSalaryScale = personal.getSalaryScale(cedula);
       JSONObject jsonScoreScale= personal.getTotalScore(cedula);
        
        try {
            jsonObject.put("salaryScale", jsonSalaryScale);
            jsonObject.put("scoreScale", jsonScoreScale);
        } catch (JSONException ex) {
            Logger.getLogger(GetSalarayScalePersonalAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
       

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());
    }
  
}
