/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Criterios_escalas_inves;
import co.org.invemar.curriculum.models.UsuariosCriteriosEscalafon;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author root
 */
@WebServlet("/ListCritheriumByGroup")
public class ListChriteriumByGroupAction  extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      JSONObject jsonObject = new JSONObject();
      
        String group = request.getParameter("group");  
        String cedula = request.getParameter("cedula");
        
        Criterios_escalas_inves listCritherium = new Criterios_escalas_inves();
        String data =listCritherium.getCritheriumByGroup(group,cedula).toString();
        
        
        
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.addHeader( "X-XSS-Protection", "1; mode=block" ); 
        response.addHeader( "X-Content-Type-Options", "nosniff" );  
       
       
        
        response.getWriter().write(data);
    }
}