/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.EstadosUsuariosEscalafon;
import co.org.invemar.curriculum.models.UsuariosCriteriosEscalafon;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
@WebServlet("/DeleteCritherium")
public class DeleteCritheriumAction extends HttpServlet {

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject jsonObject = new JSONObject();
        HttpSession session = request.getSession(false);
        String cedula = (String) session.getAttribute("cedula");
        String critherium = request.getParameter("critherium");

        try {
            if (cedula != null) {

                EstadosUsuariosEscalafon eue = new EstadosUsuariosEscalafon();
                JSONObject json = eue.getDataUser(cedula);
                String estado = json.getString("estado");            
                

                if (estado.equalsIgnoreCase("Inconcluso")) {
                    UsuariosCriteriosEscalafon uce = new UsuariosCriteriosEscalafon();
                    String pathSupport =uce.getCriterioDataPorCedulaYCriterio(cedula, critherium).getString("rutasoporte");     
                    int result = uce.deleteCritherium(cedula, critherium);
                    if (result==1) {                                            
                        
                       File file = new File(getServletContext().getRealPath("/")+pathSupport);
                       if(file.exists()){
                           file.delete();
                       }
                        
                    }
                    jsonObject.put("result", result);
                    jsonObject.put("state", "logged");
                }else{
                    jsonObject.put("result", 0);
                    jsonObject.put("state", "logged");
                }

            } else {
                jsonObject.put("state", "notlogged");
            }
        } catch (JSONException ex) {
            Logger.getLogger(DeleteCritheriumAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());

    }
}
