/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Preseleccionated;
import co.org.invemar.curriculum.pojos.AttributePreseleccionated;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
@WebServlet(name = "UpdatePreselectedAction", urlPatterns = {"/UpdatePreselected"})
public class UpdatePreselectedAction extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         JSONObject jsonObject = new JSONObject();
         
        HttpSession session = request.getSession(false);
        
        String cedula      = request.getParameter("identification");
        String name        = request.getParameter("name");
        String lastName    = request.getParameter("lastName");
        String job         = request.getParameter("job");
        String activo       = request.getParameter("activo");
        String clave         = request.getParameter("clave");
       
        try {
            if (cedula != null) {                
                
                Preseleccionated preseleccionated = new Preseleccionated();                 
                                               
                AttributePreseleccionated attributePreseleccionated = new  AttributePreseleccionated();
                
                attributePreseleccionated.setIdentification(cedula);
                attributePreseleccionated.setName(name);
                attributePreseleccionated.setLastName(lastName);
                attributePreseleccionated.setJob(job);
                attributePreseleccionated.setActive(activo);
                attributePreseleccionated.setPassword(clave);
                
                
                ResultadoTransaccion resultadoTransaccion = preseleccionated.update(attributePreseleccionated);
                jsonObject.put("result", resultadoTransaccion.getCodigo());
                jsonObject.put("state", "logged");
                jsonObject.put("message", resultadoTransaccion.getMensaje());
                
                

            } else {
                jsonObject.put("state", "notlogged");
            }
        } catch (JSONException ex) {
            Logger.getLogger(RegisteredPreseleccionatedAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());

    
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
