package co.org.invemar.curriculum.actions;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.curriculum.models.Preseleccionated;
import co.org.invemar.curriculum.utils.GestorCorreos;

@WebServlet(name = "SendEmailJefeAction", urlPatterns = {"/SendEmailJefe"})
public class SendEmailJefeAction extends HttpServlet {
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {

        try {
            JSONObject jsonObject = new JSONObject();
            response.setContentType("text/json; charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            String host = "192.168.3.2";
            String from = request.getParameter("from");
            String to = request.getParameter("to");
            String subject = request.getParameter("subject");
            String msj = request.getParameter("msj");
            
            GestorCorreos g =new GestorCorreos(host, from, to, subject, msj);
            g.manda();

        } catch (Exception ex) {
            Logger.getLogger(SendEmailJefeAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
	
	  @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }
	

}
