package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.GestionaCriterio;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 * Servlet implementation class UploadFile
 */
@WebServlet("/UploadFile")
public class UploadFile extends HttpServlet {

    private static final long serialVersionUID = 1L;

    
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("do get");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        System.out.println("starting uploading file...");
        HttpSession session= request.getSession(false);
        String cedula =(String) session.getAttribute("cedula");     
       
        
        String pathDownload="";
        JSONObject json = new JSONObject();
        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                // Parse the request
                List /* FileItem */ items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                
                String scalafon="";
                String experienceInvemar="";
                
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();
                    if (item.isFormField()) {                        
                        /*if (item.getFieldName().equalsIgnoreCase("escalafon")){
                            scalafon=item.getString();
                        }else if (item.getFieldName().equalsIgnoreCase("experiencia")) {
                            experienceInvemar=item.getString();
                        }*/
                    }
                    
                    if (!item.isFormField()) {                        
                        String criterio = item.getFieldName();                        
                        String fileName = item.getName();
                        String ext = FilenameUtils.getExtension(fileName);
                        
                        //System.out.println("Ext:"+ext);
                        
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/../../uploads");
                        if (!path.exists()) {
                            boolean status = path.mkdirs();
                        }

                        File uploadedFile = new File(path + "/" + cedula+"_criterio_"+criterio+"."+ext);
                        System.out.println("writing to disk:"+uploadedFile.getAbsolutePath());                        
                        item.write(uploadedFile);
                        
                        GestionaCriterio gc = new GestionaCriterio();                     
                        gc.registerCritherium(cedula,criterio,cedula+"_criterio_"+criterio+"."+ext,experienceInvemar,scalafon);
                         
                        
                        Random randomGenerator = new Random();                       
                        int randomInt = randomGenerator.nextInt(999);
                                
                        //json.put("file", "uploads/"+cedula+"_criterio_"+criterio+"."+ext+"?buster="+randomInt);
                        json.put("file", "descarga-soporte?nombre="+cedula+"_criterio_"+criterio+"."+ext);
                        json.put("criterio",criterio);
                        json.put("cedula",cedula);
                        
                    }
                }
                
                response.getWriter().printf(json.toString());
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
