package co.org.invemar.curriculum.actions;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import co.org.invemar.curriculum.models.Preseleccionated;
import co.org.invemar.curriculum.pojos.RevisionCriterios;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;



@WebServlet(name = "RevisarCriteriosAction", urlPatterns = {"/RevisarCriterios"})
public class RevisarCriteriosAction extends HttpServlet {
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		JSONObject jsonObject = new JSONObject();
                 
        String cc_revisor = request.getParameter("cedula_revisor");
        String cc_empleado = request.getParameter("cedula_empleado");
        String id_criterio = request.getParameter("id_criterio");
        String criterio_correcto = request.getParameter("criterio_correcto");
        String observaciones = request.getParameter("observaciones");
       
        try {
            if (cc_revisor != null) {    
                Preseleccionated preseleccionated = new Preseleccionated();  
                RevisionCriterios revisionCriterios = new  RevisionCriterios();
                
                revisionCriterios.setCedula_revisor(cc_revisor);
                revisionCriterios.setCedula_empleado(cc_empleado);
                revisionCriterios.setId_criterio(id_criterio);
                revisionCriterios.setCriterio_correcto(criterio_correcto);
                revisionCriterios.setObservaciones(observaciones);
                
                
                ResultadoTransaccion resultadoTransaccion = preseleccionated.revisionCriterios(revisionCriterios);
                jsonObject.put("result", resultadoTransaccion.getCodigo());
                jsonObject.put("state", "logged");
                jsonObject.put("message", resultadoTransaccion.getMensaje());
                
                

            } else {
                jsonObject.put("state", "notlogged");
            }
        } catch (JSONException ex) {
            Logger.getLogger(RevisarCriteriosAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
