package co.org.invemar.curriculum.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import co.org.invemar.curriculum.models.ActivacionInvestigador;
import co.org.invemar.curriculum.models.Criterios_escalas_inves;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;

@WebServlet(name = "ActivarInvestigadorAction", urlPatterns = {"/ActivarInvestigador"})
public class ActivarInvestigadorAction extends HttpServlet {
	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      JSONObject jsonObject = new JSONObject();
	      
	        String cedula = request.getParameter("cedula");
	        
	        ActivacionInvestigador activacionInvestigador = new ActivacionInvestigador();
	        String data =activacionInvestigador.changeStateSearcher(cedula).toString();
	        
	        
	        response.setContentType("text/json; charset=ISO-8859-1");
	        response.setHeader("Cache-Control", "no-cache");
	        response.addHeader( "X-XSS-Protection", "1; mode=block" ); 
	        response.addHeader( "X-Content-Type-Options", "nosniff" );  
	       
	       
	        
	        response.getWriter().write(data);
	    }
}
