/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.actions;

import co.org.invemar.curriculum.models.Criterios_escalas_inves;
import co.org.invemar.curriculum.models.EscalaSalariosEscalafon;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author root
 */
@WebServlet("/registerIdUsuerAndCritherium")
public class RegisterIDUserAndCritheriumAction extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject jsonObject = new JSONObject();

        String cedula = request.getParameter("cedula");
        String ListCritherium = request.getParameter("ListCritherium");
        String groupName = request.getParameter("groupName");
        String cantidad_soportes = request.getParameter("cantidad_soportes");
         
        ResultadoTransaccion resultadoTransaccion=null;
        
        try {
            EscalaSalariosEscalafon escalaSalariosEscalafon = new EscalaSalariosEscalafon();
            resultadoTransaccion = escalaSalariosEscalafon.registerIDUserAndCritherium(cedula, ListCritherium, groupName, cantidad_soportes);
        } catch (SQLException ex) {
            Logger.getLogger(RegisterIDUserAndCritheriumAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.addHeader("X-XSS-Protection", "1; mode=block");
        response.addHeader("X-Content-Type-Options", "nosniff");
        response.getWriter().write(String.valueOf(resultadoTransaccion.getCodigo()));

    }

}
