package co.org.invemar.curriculum.utils;


import java.util.Properties;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class GestorCorreos {
    private String a_host;
    private String a_from;
    private String a_to;
    private String a_subject;
    private String a_mensaje;
    private String error;
    
    public GestorCorreos() {
    }
   
    
    
    public GestorCorreos(String a_host, String a_from, String a_to, String a_subject, String a_mensaje) {
		super();
		this.a_host = a_host;
		this.a_from = a_from;
		this.a_to = a_to;
		this.a_subject = a_subject;
		this.a_mensaje = a_mensaje;
	}
  
    /**
      Carga el atributo a_host al objeto
     */
    public void setahost(String host) {
        a_host = host;
    }

    /**
      Carga el atributo a_from al objeto
     */
    public void setafrom(String from) {
        a_from = from;
    }

    /**
      Carga el atributo a_to al objeto
     */
    public void setato(String to) {
        a_to = to;
    }

    /**
      Carga el atributo a_subject al objeto
     */
    public void setasubject(String subject) {
        a_subject = subject;
    }

    /**
      Carga el atributo a_mensaje al objeto
     */
    public void setamensaje(String mensaje) {
        a_mensaje = mensaje;
    }

    /**
      Retorna el atributo Error del servidor de correo
     */
    public String get_error() {
        return error;
    }

    public boolean manda() {
        boolean resp = true;
        // agarra las propiedades del sistema
        Properties props = System.getProperties();
        props.put("mail.smtp.host", a_host);
        Session session = Session.getDefaultInstance(props, null);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(a_from));
            message.addRecipient(Message.RecipientType.TO,
                                 new InternetAddress(a_to));
            message.setSubject(a_subject);
            message.setContent(a_mensaje, "text/html");
            Transport.send(message);

        } catch (MessagingException mex) {
            error = mex.getMessage();
            resp = false;
            mex.printStackTrace();
            System.out.println("correo NO enviado");
        }
        System.out.println("correo enviado");
        return resp;
        
    }
}