/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.utils;

/**
 *
 * @author usrsig15
 */
public class ResultadoTransaccion {
    private int codigo;
    private int codigoError;
    private String mensaje;

    public ResultadoTransaccion() {
        codigo=-1;
        codigoError=-1;
        
    }

    
    
    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
}
