/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class GestionaCriterio {
    
    public ResultadoTransaccion registerCritherium(String idUser, String idCritherium, String supportName,String experiencieInvemar,String escalafon) throws IOException, SQLException {
       
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();

        CallableStatement st = null;

        con = cFactory.createConnection("srai");
        
        

        String sql = "{call  UTILS.registerCritherium(?,?,?,?,?,?,?)}";
        st = con.prepareCall(sql);
        st.setString(1, idUser);
        st.setString(2, idCritherium);
        st.setString(3, supportName);  
        st.setString(4, experiencieInvemar);
        st.setString(5, escalafon); 
        st.registerOutParameter(6, Types.NUMERIC);
        st.registerOutParameter(7, Types.VARCHAR);

        st.executeUpdate();

        resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigo(st.getInt(6));
        
       
        
        
        resultadoTransaccion.setCodigoError(st.getInt(6));
        resultadoTransaccion.setMensaje(st.getString(7));        
       

        con.close();
        con = null;

        return resultadoTransaccion;

    }
     public int updateExtraParameter(String cedula,String criterio,String param1,String param2){
        Connection con = null;
        int result=0;
        ConnectionFactory conectionfactory = new ConnectionFactory();        
        PreparedStatement pstmt = null;
        ResultSet rs = null;        

        String query = "UPDATE CUR_USUARIOS_ESCALAFON SET OTROS_DATOS   =?, ESCALAFON_ACTUAL=? WHERE CEDULA=?  AND ID_CRITERIO   =?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);           
            pstmt.setString(1,param1);                     
            pstmt.setString(2,param2); 
            pstmt.setString(3,cedula);
            pstmt.setString(4,criterio);
            result=pstmt.executeUpdate();      
                    

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            result=-1;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
               Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
           con=null;
        }
        return result;
     }
}
