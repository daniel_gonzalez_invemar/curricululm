package co.org.invemar.curriculum.models;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;

public class ActivacionInvestigador {
	
	 public ResultadoTransaccion changeStateSearcher(String cedula){
		 Connection con = null;
	      ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();  
	        
	        try {
	            CallableStatement st = null;
	            ConnectionFactory conectionfactory = new ConnectionFactory();
	            con = conectionfactory.createConnection("srai");

	            String sql= "UPDATE CUR_ESTADOS_USUARIOS_ESCALAFON SET ESTADO = 'I' WHERE CEDULA=?";
	        
	            st = con.prepareCall(sql);
	            st.setString(1, cedula);

	            st.execute();


	        } catch (SQLException ex) {
	            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
	            resultadoTransaccion.setMensaje(ex.getMessage());

	        } finally {
	            try {
	                con.close();
	            } catch (SQLException ex) {
	                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
	            }
	            con = null;
	        }
	        
	    	return resultadoTransaccion;
	 }

	 public JSONObject getEstadoInvestigador(String cedula){
		 ConnectionFactory conectionfactory = new ConnectionFactory();
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        Connection con = null;
	        JSONObject jo = new JSONObject();
	        //System.out.println("---action---"+ action);

	        String query = "SELECT ESTADO, AUTORIZACION FROM CUR_ESTADOS_USUARIOS_ESCALAFON WHERE CEDULA = ?";
	        try {
	            con = conectionfactory.createConnection("srai");
	            pstmt = con.prepareStatement(query);
	            pstmt.setString(1, cedula);                     
	                      
	            rs = pstmt.executeQuery();
	           
	            while (rs.next()) {                
	            	jo.put("ESTADO", rs.getString("ESTADO"));
	            	jo.put("AUTORIZACION", rs.getString("AUTORIZACION"));
	            }
	        } catch (SQLException ex) {
	            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

	        } catch (JSONException ex) {
	            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
	        } finally {
	            try {
	                con.close();
	            } catch (SQLException ex) {
	                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
	            }
	            con = null;
	        }
		 return jo; 
	 }
}
