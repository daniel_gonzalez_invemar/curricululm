/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class EscalaSalariosEscalafon  {
    
    public ResultadoTransaccion registerIDUserAndCritherium(String cedula, String ListCritherium, String groupName, String cantidad_soportes) throws IOException, SQLException{
        
       //System.out.println("!!!!!!!!!!---> "+cedula+"!!!!!!!!!!---> "+ListCritherium+"!!!!!!!!!!---> "+groupName+"!!!!!!!!!!---> "+cantidad_soportes);
        
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();

        CallableStatement st = null;

        con = cFactory.createConnection("srai");

        String sql = "{call  CURRICULUM.registerIDUserAndCritherium (?,?,?,?,?,?)}";
        st = con.prepareCall(sql);
        st.setString(1, cedula);
        st.setString(2, ListCritherium);
        st.setString(3, groupName);        
        st.registerOutParameter(4, Types.NUMERIC);
        st.registerOutParameter(5, Types.VARCHAR);
        st.setString(6, cantidad_soportes);

        st.executeUpdate();

        resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigo(st.getInt(4));
        resultadoTransaccion.setCodigoError(st.getInt(4));
        resultadoTransaccion.setMensaje(st.getString(5));
        

        con.close();
        con = null;

        return resultadoTransaccion;

    }
}
