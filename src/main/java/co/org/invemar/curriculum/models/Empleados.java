/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class Empleados extends Personal  {

    public JSONObject getDatos(String cedula){
       ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();


        String query = "SELECT cedula,nombre, apellido,clave,(select nombre_cargo from cargo_empleado where cargo=idcargo) as cargo  FROM EMPLEADOS  where cedula=?  and REGEXP_LIKE(cargo, '^[[:digit:]]+$')";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                     
           
           
            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
                jo.put("cedula", rs.getString("cedula"));
                jo.put("cargo", rs.getString("cargo"));
                jo.put("nombre", rs.getString("nombre"));
                jo.put("apellido", rs.getString("apellido"));               
                jo.put("clave", rs.getString("clave"));

            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo; 
    }
    
    public JSONObject loginEmpleados(String cedula,String clave) {

        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();


        String query = "select * from view_login_curriculum where cedula=? and clave=?";
        
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                     
            pstmt.setString(2, clave);

            rs = pstmt.executeQuery();
           
            while (rs.next()) {            
            	
                jo.put("cedula", rs.getString("cedula"));
                jo.put("cargo", rs.getString("cargo"));
                jo.put("nombre", rs.getString("nombre"));
                jo.put("apellido", rs.getString("apellido"));
                jo.put("clave", rs.getString("clave"));
                jo.put("tipo", rs.getString("tipo"));
                jo.put("activo", rs.getString("activo"));
                jo.put("revisor_curriculum",rs.getString("revisor_curriculum"));

            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo;
    }

    
}
