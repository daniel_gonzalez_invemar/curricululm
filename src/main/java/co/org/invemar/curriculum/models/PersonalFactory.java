/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

/**
 *
 * @author root
 */
public class PersonalFactory implements PersonalFactoryMethod{

    @Override
    public Personal createPersonal(String tipoPersonal) {
        if (tipoPersonal.equalsIgnoreCase("Empleados")) {
            return new Empleados();
        }else{
            return new Preseleccionated();
        }
    }
    
    
}
