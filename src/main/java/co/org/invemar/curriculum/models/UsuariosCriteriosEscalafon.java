/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class UsuariosCriteriosEscalafon {
   
    
     public int deleteCritherium(String cedula,String idCritherium){
        Connection con = null;
        int result=0;
        ConnectionFactory conectionfactory = new ConnectionFactory();        
        PreparedStatement pstmt = null;
        ResultSet rs = null;        

        String query = "DELETE FROM CUR_USUARIOS_ESCALAFON where CEDULA =? and ID_CRITERIO=?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);           
            pstmt.setString(1,cedula);                     
            pstmt.setString(2,idCritherium);      
            result=pstmt.executeUpdate();      
                    

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            result=-1;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
               Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
           con=null;
        }
        return result;
     }
    
     public JSONObject getCriterioDataPorCedulaYCriterio(String cedula, String criterio){
         ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();      

        String query = "SELECT u.CEDULA,(select estado from cur_estados_usuarios_escalafon where cedula=u.cedula) as estadousuario, u.ID_CRITERIO, u.NOMBRE_SOPORTE FROM CUR_USUARIOS_ESCALAFON  u where u.cedula=? and u.ID_CRITERIO=?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,cedula);
            pstmt.setString(2,criterio);

            
            rs = pstmt.executeQuery();
            
           
           Random randomGenerator = new Random();            
            while (rs.next()) {
                
                jo.put("cedula", rs.getString("cedula"));
                jo.put("estado", rs.getString("estadousuario"));
                jo.put("idcriterio", rs.getString("ID_CRITERIO"));          
                
                jo.put("rutasoporte", "uploads/"+rs.getString("NOMBRE_SOPORTE"));                                
                
                
            }
            

        } catch (SQLException | JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo;
     }
     
     public String getCriteriosConArchivosPorCedula(String cedula, String root) {

        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        String data="";
        
        JSONArray jsonarray = new JSONArray();

        String query = "SELECT u.CEDULA,escalafon_actual,(select estado from CUR_ESTADOS_USUARIOS_ESCALAFON where cedula=u.cedula) as estadousuario, u.ID_CRITERIO, u.NOMBRE_SOPORTE,otros_datos FROM CUR_USUARIOS_ESCALAFON  u where u.cedula=?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,cedula);

            rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	//File path = new File(root + "/../uploads/"+rs.getString("NOMBRE_SOPORTE"));
                JSONObject jo = new JSONObject();
                jo.put("cedula", rs.getString("cedula"));
                jo.put("estado", rs.getString("estadousuario"));
                jo.put("idcriterio", rs.getString("ID_CRITERIO")); 
                jo.put("param1", rs.getString("otros_datos"));         
                jo.put("param2", rs.getString("escalafon_actual"));         
                //jo.put("rutasoporte", "../uploads/"+rs.getString("NOMBRE_SOPORTE"));  
                jo.put("rutasoporte", "./descarga-soporte?nombre="+rs.getString("NOMBRE_SOPORTE"));
                jsonarray.put(jo);
            }
            data=jsonarray.toString();

        } catch (SQLException | JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return data;
    }
}
