/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class EstadosUsuariosEscalafon {

    public int updateStateUser(String cedula, String autorizacion) {
        Connection con = null;
        int result = 0;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        String query = "UPDATE CUR_ESTADOS_USUARIOS_ESCALAFON SET estado='F', autorizacion= ? WHERE CEDULA= ? ";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, autorizacion);
            pstmt.setString(2, cedula);
            result = pstmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            result = -1;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return result;
    }

    public JSONObject getDataUser(String cedula) {
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        String data = "";
        JSONObject jo = new JSONObject();

        String query = "SELECT eue.CEDULA, "
                + "  e.nombre, "
                + "  e.apellido, "
                + " decode(eue.ESTADO,'F','Termino el proceso','Inconcluso') as estado, "
                + "  eue.FECHA_REGISTRO "
                + "FROM CUR_ESTADOS_USUARIOS_ESCALAFON eue "
                + "INNER JOIN lista_escalafonamiento e "
                + "ON eue.CEDULA   =e.CEDULA "
                + "WHERE eue.cedula=?";

        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
             pstmt.setString(1, cedula);
            rs = pstmt.executeQuery();

           
            
            while (rs.next()) {

                jo.put("cedula", rs.getString("cedula"));
                jo.put("nombre", rs.getString("nombre"));
                jo.put("apellido", rs.getString("apellido"));
                jo.put("estado", rs.getString("estado"));
                jo.put("FECHA_REGISTRO", rs.getString("fecha_registro"));

            }

        } catch (SQLException | JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo;
    }

    public String getAllUser() {

        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        String data = "";

        JSONArray jsonarray = new JSONArray();

        String query = "select * from lista_escalafonamiento order by nombre asc, fecha_registro";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);

            
            
            rs = pstmt.executeQuery();           
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("cedula", rs.getString("cedula"));
                jo.put("nombre", rs.getString("nombre"));
                jo.put("apellido", rs.getString("apellido"));
                jo.put("estado", rs.getString("estado"));
                jo.put("dependencia", rs.getString("dependencia"));
                jo.put("FECHA_REGISTRO", rs.getString("fecha_registro"));
                jo.put("cargo", rs.getString("cargo"));
                jo.put("tipo", rs.getString("tipo"));
                jsonarray.put(jo);
            }
            data = jsonarray.toString();

        } catch (SQLException | JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return data;
    }
}
