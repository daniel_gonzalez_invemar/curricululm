/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.pojos.AttributePreseleccionated;
import co.org.invemar.curriculum.pojos.RevisionCriterios;
import co.org.invemar.curriculum.utils.ConnectionFactory;
import co.org.invemar.curriculum.utils.GestorCorreos;
import co.org.invemar.curriculum.utils.ResultadoTransaccion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class Preseleccionated extends Personal{
    
    @Override
    public JSONObject getDatos(String cedula) {
       ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();


        String query = "select * from CUR_PRESELECCIONADOS where cedula=?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                     
           
           
            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
                jo.put("cedula", rs.getString("cedula"));
                jo.put("cargo", rs.getString("cargo"));
                jo.put("nombre", rs.getString("nombre"));
                jo.put("apellido", rs.getString("apellido"));
                jo.put("clave", rs.getString("clave"));
                jo.put("activo", rs.getString("activo"));

            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo; 
    }
    
    public JSONArray getAllPreselected() {
       ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;        
        JSONArray jsonArray = new JSONArray();

        //String query = "select * from lista_escalafonamiento where tipo='Preseleccioanados'";
        String query = "SELECT * FROM LST_PROC_FIN_ESCALAFONAMIENTO";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);                               
           
           
            rs = pstmt.executeQuery();
            
           
            while (rs.next()) {                
                JSONObject json = new JSONObject();
                json.put("cedula", rs.getString("cedula"));
                json.put("nombre", rs.getString("nombre"));
                json.put("apellido", rs.getString("apellido"));
                json.put("estado", rs.getString("estado"));
                json.put("dependencia", rs.getString("dependencia"));
                json.put("FECHA_REGISTRO", rs.getString("fecha_registro"));
                json.put("cargo", rs.getString("cargo"));
                json.put("tipo", rs.getString("tipo"));
                jsonArray.put(json);

            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jsonArray; 
    } 

    public ResultadoTransaccion register(AttributePreseleccionated attributes) {
        Connection con = null;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();        

        try {            
            CallableStatement st = null;
            ConnectionFactory conectionfactory = new ConnectionFactory();
            con = conectionfactory.createConnection("srai");

            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$-> "+attributes);
            
            String sql = "{call  CURRICULUM.registerPreseleccionated(?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setString(1, attributes.getIdentification());
            st.setString(2, attributes.getName());
            st.setString(3, attributes.getLastName());
            st.setString(4, attributes.getJob());
            st.setString(5, attributes.getActive());
            st.setString(6, attributes.getPassword());
            st.registerOutParameter(7, Types.NUMERIC);
            st.registerOutParameter(8, Types.VARCHAR);

            st.executeUpdate();

            resultadoTransaccion = new ResultadoTransaccion();
            resultadoTransaccion.setCodigo(st.getInt(7));
            resultadoTransaccion.setCodigoError(st.getInt(7));
            resultadoTransaccion.setMensaje(st.getString(8));

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            resultadoTransaccion.setMensaje(ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return resultadoTransaccion;

    }

    
    public ResultadoTransaccion update(AttributePreseleccionated attributes) {
        Connection con = null;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();        

        try {
            CallableStatement st = null;
            ConnectionFactory conectionfactory = new ConnectionFactory();
            con = conectionfactory.createConnection("srai");

            String sql = "{call  CURRICULUM.updatePreseleccionated(?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setString(1, attributes.getIdentification());
            st.setString(2, attributes.getName());
            st.setString(3, attributes.getLastName());
            st.setString(4, attributes.getJob());
            st.setString(5, attributes.getActive());
            st.setString(6, attributes.getPassword());
            st.registerOutParameter(7, Types.NUMERIC);
            st.registerOutParameter(8, Types.VARCHAR);

            st.executeUpdate();

            resultadoTransaccion = new ResultadoTransaccion();
            resultadoTransaccion.setCodigo(st.getInt(7));
            resultadoTransaccion.setCodigoError(st.getInt(7));
            resultadoTransaccion.setMensaje(st.getString(8));

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            resultadoTransaccion.setMensaje(ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return resultadoTransaccion;

    }
    

    public JSONObject getEmailJefe(String cedula) {
    	ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();
        //System.out.println("------"+ cedula);

        String query = "SELECT CEDULA, NOMBRE || ' ' || APELLIDO EMPLEADO, NOMBRE_JEFE, EMAIL_JEFE FROM EMPLEADOS_CON_JEFE WHERE CEDULA=?";
        try {
        	//System.out.println(query);
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                     
                      
            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
            	jo.put("CEDULA", rs.getString("CEDULA"));
            	jo.put("EMPLEADO", rs.getString("EMPLEADO"));
                jo.put("NOMBRE_JEFE", rs.getString("NOMBRE_JEFE"));
                jo.put("EMAIL_JEFE", rs.getString("EMAIL_JEFE"));
                //System.out.println(jo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        //System.out.println("******************** "+jo);
        return jo; 
    	
    }
    
    
    public JSONObject getInfoRevisor(String action) {
    	ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();
        //System.out.println("---action---"+ action);

        String query = "SELECT CEDULA CC_REVISOR, NOMBRE || ' ' || APELLIDO REVISOR, EMAIL EMAIL_REVISOR FROM EMPLEADOS WHERE LOWER(REVISOR_CURRICULUM) = ?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, action);                     
                      
            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
            	jo.put("CC_REVISOR", rs.getString("CC_REVISOR"));
            	jo.put("REVISOR", rs.getString("REVISOR"));
                jo.put("EMAIL_REVISOR", rs.getString("EMAIL_REVISOR"));
               //System.out.println(jo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        //System.out.println("*****--------**** "+jo);
        return jo; 
    	
    }
    
    
    public ResultadoTransaccion revisionCriterios(RevisionCriterios criterios) {
    	Connection con = null;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();  
        
        try {
            CallableStatement st = null;
            ConnectionFactory conectionfactory = new ConnectionFactory();
            con = conectionfactory.createConnection("srai");

            String sql = "INSERT INTO CUR_REVISION_CRITERIOS(CC_REVISOR, CC_EMPLEADO, ID_CRITERIO, CRITERIO_CORRECTO, OBSERVACIONES) VALUES(?,?,?,?,?)";
        
            st = con.prepareCall(sql);
            st.setString(1, criterios.getCedula_revisor());
            st.setString(2, criterios.getCedula_empleado());
            st.setString(3, criterios.getId_criterio());
            st.setString(4, criterios.getCriterio_correcto());
            st.setString(5, criterios.getObservaciones());

            st.execute();

           /* resultadoTransaccion = new ResultadoTransaccion();
            resultadoTransaccion.setCodigo(st.getInt(7));
            resultadoTransaccion.setCodigoError(st.getInt(7));
            resultadoTransaccion.setMensaje(st.getString(8));*/

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            resultadoTransaccion.setMensaje(ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        
    	return resultadoTransaccion;
    }
    
    
    public JSONArray getRevisionCriterios(String cedula) {
    	ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONArray jsonArray = new JSONArray();
        //System.out.println("------"+ cedula);

        String query = "SELECT ID_CRITERIO, CRITERIO_CORRECTO, OBSERVACIONES FROM CUR_REVISION_CRITERIOS WHERE CC_EMPLEADO = ?";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                     
                      
            rs = pstmt.executeQuery();
           
            while (rs.next()) {   
            	JSONObject json = new JSONObject();
            	json.put("ID_CRITERIO", rs.getString("ID_CRITERIO"));
            	json.put("CRITERIO_CORRECTO", rs.getString("CRITERIO_CORRECTO"));
            	json.put("OBSERVACIONES", rs.getString("OBSERVACIONES"));
            	jsonArray.put(json);
            }
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        //System.out.println("******************** "+jsonArray);
        return jsonArray; 
    	
    }
}
