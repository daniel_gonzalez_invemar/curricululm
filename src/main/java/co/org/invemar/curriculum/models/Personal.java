/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public abstract class Personal {
    
    public abstract JSONObject getDatos(String cedula);
    
    
     public  JSONObject getTotalScore(String cedula){
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();


        String query = "select curriculum.getScoreSalaryScaleByCedula(?) as totalScore from dual";
        
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                                

            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
                jo.put("totalscore", rs.getString("totalscore"));
                
            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo;
     }
    
    
    
    public  JSONObject getSalaryScale(String cedula){
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONObject jo = new JSONObject();


        String query = "select curriculum.getSalaryScaleByCedula(?) as scale from dual";
        
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, cedula);                                

            rs = pstmt.executeQuery();
           
            while (rs.next()) {                
                jo.put("scale", rs.getString("scale"));
                
            }
          

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        return jo;
    }
    
}
