/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.curriculum.models;

import co.org.invemar.curriculum.utils.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class Criterios_escalas_inves {
      public JSONObject getCritheriumByGroup(String groupName,String id){
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonCritherium = new JSONObject(); 

        String query = "select id, nombre, valor, grupo, curriculum.isApplyToID(id,?) as state, curriculum.getNroSoporte(id,?) as soportes from CUR_CRITERIOS_ESCALAS_INVESTI where grupo=? order by id";
        //String query = "select id, nombre, valor, grupo,curriculum.isApplyToID(id,?) as state  from CUR_CRITERIOS_ESCALAS_INVESTI where grupo=? order by id";
        try {
            con = conectionfactory.createConnection("srai");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, id);
            pstmt.setString(2, id);  
            pstmt.setString(3, groupName);   
           
            rs = pstmt.executeQuery();
          
            while (rs.next()) {
                JSONObject jo = new JSONObject();  
                jo.put("estado",rs.getString("state"));
                jo.put("id", rs.getString("id"));
                jo.put("name", rs.getString("nombre"));
                jo.put("value", rs.getDouble("valor"));
                jo.put("group", rs.getString("grupo"));
                jo.put("soportes", rs.getString("soportes")); 
                jsonArray.put(jo);
               // System.out.println("***********> "+ jsonArray);
            }
            jsonCritherium.put("critherium",jsonArray) ; 

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            con = null;
        }
        //System.out.println("***********> "+ jsonCritherium);
        return jsonCritherium;
                
      }
}
