package co.org.invemar.curriculum.pojos;



public class RevisionCriterios {
	private String cedula_revisor="";
	private String cedula_empleado="";
	private String id_criterio="";
	private String criterio_correcto="";
	private String observaciones;
	
	
	public String getCedula_revisor() {
		return cedula_revisor;
	}
	
	public void setCedula_revisor(String cedula_revisor) {
		this.cedula_revisor = cedula_revisor;
	}
	
	public String getCedula_empleado() {
		return cedula_empleado;
	}
	
	public void setCedula_empleado(String cedula_empleado) {
		this.cedula_empleado = cedula_empleado;
	}
	
	public String getId_criterio() {
		return id_criterio;
	}
	
	public void setId_criterio(String id_criterio) {
		this.id_criterio = id_criterio;
	}
	
	public String getCriterio_correcto() {
		return criterio_correcto;
	}
	
	public void setCriterio_correcto(String criterio_correcto) {
		this.criterio_correcto = criterio_correcto;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}
